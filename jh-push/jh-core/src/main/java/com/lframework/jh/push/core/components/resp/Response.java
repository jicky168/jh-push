package com.lframework.jh.push.core.components.resp;

import java.io.Serializable;

/**
 * 响应数据
 */
public interface Response<T> extends Serializable {

}
