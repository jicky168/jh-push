package com.lframework.jh.push.core.controller;


import com.lframework.jh.push.core.utils.JsonUtil;
import com.lframework.jh.push.core.utils.StringUtil;
import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.OpenApiReqVo;

/**
 * 具有Security能力的BaseController
 *
 * @author zmj
 */
public abstract class DefaultBaseController extends BaseController {

    public <T extends BaseVo> T getOpenApiVo(OpenApiReqVo vo, Class<T> clazz) {
        if (StringUtil.isBlank(vo.getParams())) {
            return null;
        }
        return JsonUtil.parseObject(vo.getParams(), clazz);
    }
}
