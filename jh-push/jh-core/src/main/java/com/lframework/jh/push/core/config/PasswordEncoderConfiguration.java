package com.lframework.jh.push.core.config;

import com.lframework.jh.push.core.config.security.PasswordEncoderWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncoderConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PasswordEncoderWrapper passwordEncoderWrapper() {
        return new PasswordEncoderWrapper(passwordEncoder());
    }
}
