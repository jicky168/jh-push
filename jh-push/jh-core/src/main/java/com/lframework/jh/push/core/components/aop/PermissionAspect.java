package com.lframework.jh.push.core.components.aop;

import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.security.CheckPermissionHandler;
import com.lframework.jh.push.core.exceptions.impl.AccessDeniedException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * 鉴权切面
 *
 * @author zmj
 */
@Aspect
public class PermissionAspect {

    private CheckPermissionHandler checkPermissionHandler;

    public PermissionAspect(CheckPermissionHandler checkPermissionHandler) {
        this.checkPermissionHandler = checkPermissionHandler;
    }

    @Pointcut("@annotation(com.lframework.jh.push.core.annotations.security.HasPermission) && execution(public * *(..))")
    public void permissionPointCut() {

    }

    @Around(value = "permissionPointCut()")
    public Object permission(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        HasPermission hasPermission = signature.getMethod().getAnnotation(HasPermission.class);

        if (!checkPermissionHandler.valid(hasPermission.calcType(), hasPermission.value())) {
            throw new AccessDeniedException("暂无权限，请联系系统管理员！");
        }

        Object value = joinPoint.proceed();

        return value;
    }
}
