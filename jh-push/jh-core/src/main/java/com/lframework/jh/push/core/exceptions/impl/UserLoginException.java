package com.lframework.jh.push.core.exceptions.impl;


import com.lframework.jh.push.core.constants.ResponseConstants;
import com.lframework.jh.push.core.exceptions.ClientException;

public class UserLoginException extends ClientException {

    public UserLoginException(String msg) {

        super(ResponseConstants.INVOKE_RESULT_FAIL_USER_LOGIN_FAIL, msg);
    }
}
