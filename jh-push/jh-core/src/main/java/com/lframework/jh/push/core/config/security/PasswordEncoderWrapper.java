package com.lframework.jh.push.core.config.security;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 登陆密码Encoder Wrapper 用于包装Encoder 方便统一管理
 *
 * @author zmj
 */
public class PasswordEncoderWrapper {

    private PasswordEncoder encoder;

    public PasswordEncoderWrapper(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    public PasswordEncoder getEncoder() {

        return encoder;
    }
}
