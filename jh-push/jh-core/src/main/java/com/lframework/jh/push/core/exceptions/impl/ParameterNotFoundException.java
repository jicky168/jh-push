package com.lframework.jh.push.core.exceptions.impl;

public class ParameterNotFoundException extends DefaultSysException {

    public ParameterNotFoundException() {
    }

    public ParameterNotFoundException(String msg) {
        super(msg);
    }
}
