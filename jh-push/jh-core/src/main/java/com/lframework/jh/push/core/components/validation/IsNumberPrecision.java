package com.lframework.jh.push.core.components.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 校验小数位数
 *
 * @author zmj
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = NumberPrecisionValidator.class)
public @interface IsNumberPrecision {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int value();
}