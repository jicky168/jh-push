package com.lframework.jh.push.core.impl;

import com.lframework.jh.push.core.dto.UserDto;
import com.lframework.jh.push.core.mappers.UserMapper;
import com.lframework.jh.push.core.service.UserService;
import com.lframework.jh.push.core.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * 默认用户Service实现
 *
 * @author zmj
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Cacheable(value = UserDto.CACHE_NAME, key = "#id", unless = "#result == null")
    @Override
    public UserDto findById(String id) {

        if (StringUtil.isBlank(id)) {
            return null;
        }

        return userMapper.findById(id);
    }

    @Override
    public UserDto findByCode(String code) {
        return userMapper.findByCode(code);
    }

    @CacheEvict(value = {UserDto.CACHE_NAME}, key = "#key")
    @Override
    public void cleanCacheByKey(Serializable key) {

    }
}
