package com.lframework.jh.push.core.constants;

public class MqConstants {

    /**
     * 重试通知的间隔时间，单位：秒
     */
    public static final int[] NOTIFY_RETRY_SECONDS = new int[]{
            3, 5, 10, 30, 60, 120, 300, 1200, 1800, 3600
    };

    /**
     * 异步查询短信结果重试的间隔时间，单位：秒
     */
    public static final int[] SMS_ASYNC_QUERY_RETRY_SECONDS = new int[]{
            10, 30, 60, 120, 300, 1200, 1800, 3600
    };

    /**
     * 查询短信推送结果（明细方式）
     */
    public static final String QUEUE_QUERY_SMS_PUSH_RESULT_DETAIL = "queue_query_sms_push_result_detail";

    /**
     * 查询短信推送结果（分页方式）
     */
    public static final String QUEUE_QUERY_SMS_PUSH_RESULT_PAGE = "queue_query_sms_push_result_page";

    /**
     * 短信批次通知
     */
    public static final String QUEUE_NOTIFY_SMS_BATCH = "queue_notify_sms_batch";
}
