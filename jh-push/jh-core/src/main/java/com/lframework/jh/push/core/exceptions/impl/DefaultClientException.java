package com.lframework.jh.push.core.exceptions.impl;


import com.lframework.jh.push.core.constants.ResponseConstants;
import com.lframework.jh.push.core.exceptions.ClientException;

/**
 * 自定义消息的异常
 *
 * @author zmj
 */
public class DefaultClientException extends ClientException {

    public DefaultClientException(String msg) {

        super(ResponseConstants.INVOKE_RESULT_FAIL_CODE, msg);
    }
}
