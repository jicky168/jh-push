package com.lframework.jh.push.core.components.validation;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TypeMismatch {

    String message() default "数据格式有误！";
}
