package com.lframework.jh.push.core.components.sign;


import com.lframework.jh.push.core.utils.IdUtil;
import com.lframework.jh.push.core.vo.OpenApiReqVo;
import java.util.HashMap;
import java.util.Map;

public interface CheckSignHandler {

    /**
     * 包装参数
     *
     * @return
     */
    default Map<String, String> wrap(Integer mchId, String json) {
        Map<String, String> req = new HashMap<>(5);
        req.put("mchId", mchId.toString());
        req.put("timestamp", String.valueOf(System.currentTimeMillis()));
        req.put("nonceStr", IdUtil.getUUID());
        req.put("params", json);

        return req;
    }

    /**
     * 验签
     *
     * @param req
     * @return
     */
    boolean check(OpenApiReqVo req);

    /**
     * 加签
     *
     * @return
     */
    String sign(Integer mchId, String timestamp, String nonceStr, String json);

    /**
     * 加签
     *
     * @return
     */
    String sign(Integer mchId, String apiSecret, String timestamp, String nonceStr,
        String json);
}
