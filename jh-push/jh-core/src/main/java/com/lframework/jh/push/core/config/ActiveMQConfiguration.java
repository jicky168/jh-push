package com.lframework.jh.push.core.config;

import com.lframework.jh.push.core.components.mq.ActiveMqProducer;
import com.lframework.jh.push.core.components.mq.MqProducer;
import com.lframework.jh.push.core.constants.MqConstants;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@Configuration
public class ActiveMQConfiguration {

    @Bean
    public MqProducer mqProducer() {
        return new ActiveMqProducer();
    }

    @Bean
    public ActiveMQQueue querySmsPushResult() {
        return new ActiveMQQueue(MqConstants.QUEUE_QUERY_SMS_PUSH_RESULT_DETAIL);
    }
}
