package com.lframework.jh.push.core.annotations.convert;

import java.lang.annotation.*;

/**
 * Dto转Bo时，是否为Enum字段
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnumConvert {

}
