package com.lframework.jh.push.core.components.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 编号类型
 *
 * @author zmj
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = CodeValidator.class)
public @interface IsCode {

    Class<?>[] groups() default {};

    String message() default "编号必须由字母、数字、“-_.”组成，长度不能超过20位";

    Class<? extends Payload>[] payload() default {};
}
