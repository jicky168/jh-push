package com.lframework.jh.push.core.config;

import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import com.lframework.jh.push.core.components.security.PermitAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Map.Entry;
import java.util.stream.Collectors;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Autowired
    private PermitAllService permitAllService;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**")
                .excludePathPatterns(
                        permitAllService.getUrls().stream().map(Entry::getValue).collect(Collectors.toList()));
    }
}
