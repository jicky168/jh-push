package com.lframework.jh.push.core.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户Dto
 *
 * @author zmj
 */
@Data
public class UserDto implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    public static final String CACHE_NAME = "UserDto";

    /**
     * ID
     */
    private String id;

    /**
     * 编号
     */
    private String code;

    /**
     * 姓名
     */
    private String name;
}
