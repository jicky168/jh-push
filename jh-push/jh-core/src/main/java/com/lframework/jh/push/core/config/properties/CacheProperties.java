package com.lframework.jh.push.core.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Data
@ConfigurationProperties(prefix = "jh.cache")
public class CacheProperties {

    /**
     * 公共缓存过期时间
     */
    private Long ttl = 1800L;

    /**
     * 特殊指定缓存过期时间
     */
    private Map<String, Long> regions;
}
