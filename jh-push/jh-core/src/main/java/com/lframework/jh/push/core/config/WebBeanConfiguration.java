package com.lframework.jh.push.core.config;

import com.lframework.jh.push.core.components.aop.ControllerAspector;
import com.lframework.jh.push.core.components.aop.OpenApiAspect;
import com.lframework.jh.push.core.components.aop.PermissionAspect;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilderWrapper;
import com.lframework.jh.push.core.components.resp.ResponseBuilder;
import com.lframework.jh.push.core.components.security.CheckPermissionHandler;
import com.lframework.jh.push.core.components.security.UserTokenResolver;
import com.lframework.jh.push.core.components.security.UserTokenResolverImpl;
import com.lframework.jh.push.core.components.sign.CheckSignFactory;
import com.lframework.jh.push.core.components.sign.CheckSignHandler;
import com.lframework.jh.push.core.components.sign.DefaultCheckSignFactory;
import com.lframework.jh.push.core.components.sign.DefaultCheckSignHandler;
import com.lframework.jh.push.core.utils.ApplicationUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebBeanConfiguration {

    @Bean
    public ControllerAspector controllerAspector() {

        return new ControllerAspector();
    }

    @Bean
    public OpenApiAspect openApiAspect() {
        return new OpenApiAspect();
    }

    @Bean
    public PermissionAspect permissionAspect(CheckPermissionHandler checkPermissionHandler) {
        return new PermissionAspect(checkPermissionHandler);
    }

    @Bean
    public ResponseBuilder invokeResultBuilderWrapper() {
        return new InvokeResultBuilderWrapper();
    }

    @Bean
    public CheckSignFactory defaultCheckSignFactory() {
        return new DefaultCheckSignFactory();
    }

    @Bean
    public UserTokenResolver userTokenResolver() {
        return new UserTokenResolverImpl();
    }

    @Bean
    public ApplicationUtil applicationUtil() {
        return new ApplicationUtil();
    }

    @Bean
    @ConditionalOnMissingBean(CheckSignHandler.class)
    public CheckSignHandler checkSignHandler() {
        return new DefaultCheckSignHandler();
    }
}
