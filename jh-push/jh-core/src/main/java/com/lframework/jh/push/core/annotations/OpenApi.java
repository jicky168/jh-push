package com.lframework.jh.push.core.annotations;

import java.lang.annotation.*;

/**
 * 是否为开放API
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpenApi {

    /**
     * 是否验证签名
     *
     * @return
     */
    boolean sign() default false;
}
