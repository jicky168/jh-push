package com.lframework.jh.push.core.annotations.convert;

import java.lang.annotation.*;

/**
 * Dto转Bo时，是否忽略转换
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreConvert {

}
