package com.lframework.jh.push.core.utils;

/**
 * ID工具类
 *
 * @author zmj
 */
public class IdUtil {

    /**
     * 获取ID（雪花算法）
     *
     * @return
     */
    public static String getId() {

        IdWorker idWorker = ApplicationUtil.getBean(IdWorker.class);
        return idWorker.nextIdStr();
    }

    /**
     * 获取UUID
     *
     * @return
     */
    public static String getUUID() {
        return cn.hutool.core.util.IdUtil.fastSimpleUUID();
    }

    /**
     * 获取推送单ID
     *
     * @return
     */
    public static String getOrderId() {
        return "P" + getId();
    }

    /**
     * 获取批次号
     * @return
     */
    public static String getBatchNo() {
        return "B" + getId();
    }
}
