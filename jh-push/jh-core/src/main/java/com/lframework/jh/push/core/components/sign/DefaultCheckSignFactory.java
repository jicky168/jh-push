package com.lframework.jh.push.core.components.sign;


import com.lframework.jh.push.core.utils.ApplicationUtil;

public class DefaultCheckSignFactory implements CheckSignFactory {

    @Override
    public CheckSignHandler getInstance() {
        return ApplicationUtil.getBean(CheckSignHandler.class);
    }
}
