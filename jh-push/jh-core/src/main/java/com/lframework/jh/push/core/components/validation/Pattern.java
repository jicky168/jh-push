package com.lframework.jh.push.core.components.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = PatternValidator.class)
public @interface Pattern {

    /**
     * 正则表达式
     *
     * @return
     */
    String regexp();

    Class<?>[] groups() default {};

    String message();

    Class<? extends Payload>[] payload() default {};
}
