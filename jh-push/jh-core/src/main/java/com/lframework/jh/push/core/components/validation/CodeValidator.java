package com.lframework.jh.push.core.components.validation;


import com.lframework.jh.push.core.constants.PatternPool;
import com.lframework.jh.push.core.utils.RegUtil;
import com.lframework.jh.push.core.utils.StringUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 编号校验 如果参数是null或empty 则通过校验
 *
 * @author zmj
 */
public class CodeValidator implements ConstraintValidator<IsCode, CharSequence> {

    @Override
    public boolean isValid(CharSequence charSequence,
                           ConstraintValidatorContext constraintValidatorContext) {

        return StringUtil.isEmpty(charSequence) || RegUtil.isMatch(PatternPool.PATTERN_CODE,
                charSequence.toString());
    }
}
