package com.lframework.jh.push.core.annotations.security;


import com.lframework.jh.push.core.components.security.PermissionCalcType;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HasPermission {

    /**
     * 权限
     *
     * @return
     */
    String[] value();

    /**
     * 计算方式
     *
     * @return
     */
    PermissionCalcType calcType() default PermissionCalcType.OR;
}
