package com.lframework.jh.push.core.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "jh.web")
public class WebProperties {

    /**
     * 终端ID
     */
    private Long workerId = -1L;

    /**
     * 数据中心ID
     */
    private Long centerId = -1L;
}
