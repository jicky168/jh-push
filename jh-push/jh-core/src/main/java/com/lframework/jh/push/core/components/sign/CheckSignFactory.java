package com.lframework.jh.push.core.components.sign;

public interface CheckSignFactory {

    CheckSignHandler getInstance();
}
