package com.lframework.jh.push.core.components.sign;


import com.lframework.jh.push.core.vo.OpenApiReqVo;

public class DefaultCheckSignHandler implements CheckSignHandler {

    @Override
    public boolean check(OpenApiReqVo req) {

        return false;
    }

    @Override
    public String sign(Integer mchId, String timestamp, String nonceStr, String json) {
        return null;
    }

    @Override
    public String sign(Integer mchId, String apiSecret, String timestamp, String nonceStr, String json) {
        return null;
    }
}
