package com.lframework.jh.push.core.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.lframework.jh.push.core.config.handlers.DefaultBaseEntityFillHandler;
import com.lframework.jh.push.core.config.injectors.MybatisPlusUpdateAllColumnInjector;
import com.lframework.jh.push.core.config.properties.MybatisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({MybatisProperties.class})
public class MybatisConfiguration {

    @Bean
    public MetaObjectHandler getMetaObjectHandler(MybatisProperties properties) {

        return new DefaultBaseEntityFillHandler(properties);
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        return interceptor;
    }

    @Bean
    public MybatisPlusUpdateAllColumnInjector mybatisPlusUpdateAllColumnInjector() {
        return new MybatisPlusUpdateAllColumnInjector();
    }
}
