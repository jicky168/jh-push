package com.lframework.jh.push.core.exceptions.impl;


import com.lframework.jh.push.core.constants.ResponseConstants;
import com.lframework.jh.push.core.exceptions.ClientException;

/**
 * 登录状态过期导致的异常
 *
 * @author zmj
 */
public class AuthExpiredException extends ClientException {

    public AuthExpiredException() {

        super(ResponseConstants.INVOKE_RESULT_FAIL_CODE_AUTH_EXPIRED,
                ResponseConstants.INVOKE_RESULT_ERROR_MSG_AUTH_EXPIRED);
    }
}
