package com.lframework.jh.push.core.vo;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@Data
public class OpenApiReqVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商户ID
     */
    @ApiModelProperty(value = "商户ID", required = true)
    private Integer mchId;

    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳", required = true)
    private String timestamp;

    /**
     * 随机数
     */
    @ApiModelProperty(value = "随机数", required = true)
    private String nonceStr;

    /**
     * 签名
     */
    @ApiModelProperty(value = "签名", required = true)
    private String sign;

    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数", required = true)
    private String params;
}
