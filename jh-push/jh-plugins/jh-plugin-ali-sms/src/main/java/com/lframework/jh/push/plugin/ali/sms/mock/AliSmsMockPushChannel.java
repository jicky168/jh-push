package com.lframework.jh.push.plugin.ali.sms.mock;

import cn.hutool.core.util.RandomUtil;
import com.lframework.jh.push.core.utils.IdUtil;
import com.lframework.jh.push.plugin.ali.sms.AliSmsPushChannel;
import com.lframework.jh.push.plugin.ali.sms.AliSmsPushChannelParams;
import com.lframework.jh.push.plugin.ali.sms.AliSmsPushDetailResp;
import com.lframework.jh.push.plugin.ali.sms.AliSmsPushResp;
import com.lframework.jh.push.plugin.core.SmsPushChannel;
import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import com.lframework.jh.push.plugin.core.SmsPushResp;
import com.lframework.jh.push.plugin.core.constants.PushConstants;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultDetailDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultDetailRespDto;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component("ali_sms" + PushConstants.MOCK_SMS_PUSH_CHANNEL_BEAN_NAME_SUFFIX)
public class AliSmsMockPushChannel extends AliSmsPushChannel implements SmsPushChannel {

    @Override
    public Class<? extends SmsPushChannelParams> getClazz() {
        return AliSmsPushChannelParams.class;
    }

    @Override
    public SmsPushResp push(SmsPushChannelParams params) throws Exception {
        AliSmsPushResp resp = new AliSmsPushResp();
        resp.setRequestId(IdUtil.getUUID());
        String bizId = IdUtil.getUUID();
        List<AliSmsPushDetailResp> detailResps = new ArrayList<>();
        String[] phoneNumbers = params.getPhoneNumbers().split(",");
        for (String phoneNumber : phoneNumbers) {
            AliSmsPushDetailResp detailResp = new AliSmsPushDetailResp();
            detailResp.setBizId(bizId);
            detailResp.setPhoneNumber(phoneNumber);
            detailResps.add(detailResp);
        }

        resp.setDetails(detailResps);

        Thread.sleep(RandomUtil.randomLong(2000));
        return resp;
    }

    @Override
    public QuerySmsPushResultDetailRespDto query(QuerySmsPushResultDetailDto dto) throws Exception {

        QuerySmsPushResultDetailRespDto result = new QuerySmsPushResultDetailRespDto();
        result.setPhoneNumber(dto.getPhoneNumber());
        result.setStatus(3);
        result.setSuccessTime(LocalDateTime.now());
        result.setContent("【阿里云】模拟环境短信");

        Thread.sleep(RandomUtil.randomLong(2000));

        return result;
    }
}
