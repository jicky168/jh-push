package com.lframework.jh.push.plugin.ali.sms;

import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import lombok.Data;

@Data
public class AliSmsPushChannelParams extends SmsPushChannelParams {

    private static final long serialVersionUID = 1L;

    /**
     * accessKeyId
     */
    public String accessKeyId;

    /**
     * accessKeySecret
     */
    public String accessKeySecret;
}
