package com.lframework.jh.push.plugin.ali.sms;

import com.lframework.jh.push.plugin.core.SmsPushDetailResp;
import lombok.Data;

@Data
public class AliSmsPushDetailResp extends SmsPushDetailResp {

}
