package com.lframework.jh.push.plugin.ali.sms;

import com.lframework.jh.push.plugin.core.SmsPushResp;
import lombok.Data;

@Data
public class AliSmsPushResp extends SmsPushResp {

    /**
     * 请求ID
     */
    private String requestId;
}
