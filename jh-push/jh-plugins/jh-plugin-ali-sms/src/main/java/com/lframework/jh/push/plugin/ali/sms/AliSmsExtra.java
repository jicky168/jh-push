package com.lframework.jh.push.plugin.ali.sms;

import lombok.Data;

@Data
public class AliSmsExtra {

    /**
     * 短信签名名称
     */
    private String signName;

    /**
     * 短信模板CODE
     */
    private String templateCode;

    /**
     * 短信模板变量对应的实际值
     */
    private String templateParam;
}
