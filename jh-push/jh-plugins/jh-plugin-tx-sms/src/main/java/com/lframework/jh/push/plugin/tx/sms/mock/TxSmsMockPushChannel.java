package com.lframework.jh.push.plugin.tx.sms.mock;

import cn.hutool.core.util.RandomUtil;
import com.lframework.jh.push.core.utils.IdUtil;
import com.lframework.jh.push.plugin.core.SmsPushChannel;
import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import com.lframework.jh.push.plugin.core.SmsPushResp;
import com.lframework.jh.push.plugin.core.constants.PushConstants;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultDetailDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultDetailRespDto;
import com.lframework.jh.push.plugin.tx.sms.TxSmsPushChannel;
import com.lframework.jh.push.plugin.tx.sms.TxSmsPushChannelParams;
import com.lframework.jh.push.plugin.tx.sms.TxSmsPushDetailResp;
import com.lframework.jh.push.plugin.tx.sms.TxSmsPushResp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component("tx_sms" + PushConstants.MOCK_SMS_PUSH_CHANNEL_BEAN_NAME_SUFFIX)
public class TxSmsMockPushChannel extends TxSmsPushChannel implements SmsPushChannel {

    @Override
    public Class<? extends SmsPushChannelParams> getClazz() {
        return TxSmsPushChannelParams.class;
    }

    @Override
    public SmsPushResp push(SmsPushChannelParams params) throws Exception {

        TxSmsPushResp resp = new TxSmsPushResp();
        List<TxSmsPushDetailResp> detailResps = new ArrayList<>();
        String[] phoneNumbers = params.getPhoneNumbers().split(",");
        for (String phoneNumber : phoneNumbers) {
            TxSmsPushDetailResp detailResp = new TxSmsPushDetailResp();
            detailResp.setBizId(IdUtil.getUUID());
            detailResp.setPhoneNumber(phoneNumber);

            detailResps.add(detailResp);
        }

        resp.setDetails(detailResps);

        Thread.sleep(RandomUtil.randomLong(2000));

        return resp;
    }

    @Override
    public QuerySmsPushResultDetailRespDto query(QuerySmsPushResultDetailDto dto) throws Exception {
        QuerySmsPushResultDetailRespDto respDto = new QuerySmsPushResultDetailRespDto();
        respDto.setStatus(3);
        respDto.setPhoneNumber(dto.getPhoneNumber());
        respDto.setSuccessTime(LocalDateTime.now());

        Thread.sleep(RandomUtil.randomLong(2000));

        return respDto;
    }
}
