package com.lframework.jh.push.plugin.tx.sms;

import com.lframework.jh.push.plugin.core.SmsPushResp;
import lombok.Data;

@Data
public class TxSmsPushResp extends SmsPushResp {

}
