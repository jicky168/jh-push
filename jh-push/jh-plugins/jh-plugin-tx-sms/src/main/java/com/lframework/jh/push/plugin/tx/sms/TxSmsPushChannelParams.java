package com.lframework.jh.push.plugin.tx.sms;

import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import lombok.Data;

@Data
public class TxSmsPushChannelParams extends SmsPushChannelParams {

    private static final long serialVersionUID = 1L;

    /**
     * secretId
     */
    private String secretId;

    /**
     * secretKey
     */
    private String secretKey;

    /**
     * 地域
     */
    private String region;

    /**
     * 应用ID
     */
    private String sdkAppId;
}
