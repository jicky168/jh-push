package com.lframework.jh.push.plugin.tx.sms;

import com.lframework.jh.push.plugin.core.SmsPushDetailResp;
import com.lframework.jh.push.plugin.core.SmsPushResp;
import lombok.Data;

@Data
public class TxSmsPushDetailResp extends SmsPushDetailResp {

}
