package com.lframework.jh.push.plugin.core.dto;

import com.lframework.jh.push.core.dto.BaseDto;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Data;

@Data
public class QuerySmsPushResultPageRespDto implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 是否有下一页
     */
    private boolean hasNext;

    /**
     * 明细数据
     */
    private List<DetailDto> details;

    @Data
    public static class DetailDto implements BaseDto, Serializable {

        private static final long serialVersionUID = 1L;

        /**
         * 手机号
         */
        private String phoneNumber;

        /**
         * 状态 0、未查到结果 1、发送中 2、发送失败 3、发送成功
         */
        private Integer status;

        /**
         * 发送成功时间，只有status=3时有值
         */
        private LocalDateTime successTime;

        /**
         * 短信内容，只有status=3时可能有值，还需要看渠道是否有返回
         */
        private String content;

        /**
         * 失败原因，只有status=2时可能有值
         */
        private String errorMsg;
    }
}
