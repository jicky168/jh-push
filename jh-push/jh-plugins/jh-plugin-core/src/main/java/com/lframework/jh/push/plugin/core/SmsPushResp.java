package com.lframework.jh.push.plugin.core;

import java.util.List;
import lombok.Data;

/**
 * 短信推送返回值
 */
@Data
public abstract class SmsPushResp {

    /**
     * 业务ID 当查询方式为明细方式时此值为空，以details内的bizId为准
     */
    private String bizId;

    /**
     * 明细数据
     */
    private List<? extends SmsPushDetailResp> details;
}
