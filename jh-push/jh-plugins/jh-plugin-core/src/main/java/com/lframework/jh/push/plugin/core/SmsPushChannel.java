package com.lframework.jh.push.plugin.core;

import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultDetailDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultPageDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultDetailRespDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultPageRespDto;
import com.lframework.jh.push.plugin.core.enums.SmsQueryType;
import java.util.List;

/**
 * 短信推送渠道
 */
public interface SmsPushChannel {

    /**
     * 获取配置参数的Class
     *
     * @return
     */
    Class<? extends SmsPushChannelParams> getClazz();

    /**
     * 推送
     *
     * @param params
     * @return
     */
    SmsPushResp push(SmsPushChannelParams params) throws Exception;

    /**
     * 查询（明细方式）
     *
     * @param dto
     * @return
     */
    QuerySmsPushResultDetailRespDto query(QuerySmsPushResultDetailDto dto) throws Exception;

    /**
     * 查询（分页方式）
     *
     * @param dto
     * @return
     * @throws Exception
     */
    QuerySmsPushResultPageRespDto query(QuerySmsPushResultPageDto dto) throws Exception;

    /**
     * 校验扩展参数
     *
     * @param extra
     */
    default void validExtra(String extra) {

    }

    /**
     * 单次支持最大短信条数
     *
     * @return
     */
    Integer allowSmsCount();

    /**
     * 查询方式
     *
     * @return
     */
    SmsQueryType getQueryType();
}
