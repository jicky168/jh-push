package com.lframework.jh.push.plugin.core;

import lombok.Data;

@Data
public class EmptySmsPushChannelParams extends SmsPushChannelParams {

    private static final long serialVersionUID = 1L;

    /**
     * 接收手机号
     */
    private String phoneNumbers;

    /**
     * 扩展参数
     */
    private String extra;

    /**
     * 推送单ID
     */
    private String orderId;
}
