package com.lframework.jh.push.plugin.core.enums;

public enum SmsQueryType {
    /**
     * 单条查询
     */
    DETAIL,
    /**
     * 分页查询
     */
    PAGE;
}
