package com.lframework.jh.push.plugin.core.dto;

import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class QuerySmsPushResultDetailDto implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前次数
     */
    private int curIndex;

    /**
     * 推送单ID
     */
    private String orderId;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 业务ID
     */
    private String bizId;

    /**
     * 配置参数
     */
    private SmsPushChannelParams params;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 推送时间
     */
    private LocalDateTime pushTime;

    /**
     * 短信内容
     */
    private String content;
}
