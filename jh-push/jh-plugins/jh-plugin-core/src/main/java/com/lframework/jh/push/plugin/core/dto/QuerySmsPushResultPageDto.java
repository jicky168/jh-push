package com.lframework.jh.push.plugin.core.dto;

import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QuerySmsPushResultPageDto implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前次数
     */
    private int curIndex;

    /**
     * 当前页码
     */
    private long pageIndex;

    /**
     * 推送单ID
     */
    private String orderId;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 业务ID
     */
    private String bizId;

    /**
     * 配置参数
     */
    private SmsPushChannelParams params;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 推送时间
     */
    private LocalDateTime pushTime;

    /**
     * 短信内容
     */
    private String content;
}
