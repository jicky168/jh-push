package com.lframework.jh.push.plugin.core;

import java.io.Serializable;
import lombok.Data;

/**
 * 短信推送-渠道配置参数
 */
@Data
public abstract class SmsPushChannelParams implements Serializable {

    /**
     * 接收手机号
     */
    private String phoneNumbers;

    /**
     * 扩展参数
     */
    private String extra;

    /**
     * 推送单ID
     */
    private String orderId;
}
