package com.lframework.jh.push.plugin.core;

import lombok.Data;

/**
 * 短信推送返回值
 */
@Data
public abstract class SmsPushDetailResp {

    /**
     * 业务ID 当查询方式为分页方式时此值为空，以resp内的bizId为准
     */
    private String bizId;

    /**
     * 渠道标识 0失败 1成功
     */
    private int channelFlag = 1;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 错误信息
     */
    private String errorMsg;
}
