package com.lframework.jh.push.plugin.core.dto;

import com.lframework.jh.push.core.dto.BaseDto;
import lombok.Data;

import java.io.Serializable;

@Data
public class NotifySmsBatchDto implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 通知消息ID
     */
    private String notifyId;


    /**
     * 推送单ID
     */
    private String orderId;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 外部编号
     */
    private String outerNo;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 当前通知次数
     */
    private int currentIndex;
}
