package com.lframework.jh.push.plugin.core.constants;

public interface PushConstants {
    String SMS_PUSH_CHANNEL_BEAN_NAME_SUFFIX = "_sms_push_channel";

    String MOCK_SMS_PUSH_CHANNEL_BEAN_NAME_SUFFIX = "_mock_sms_push_channel";
}
