package com.lframework.jh.push.api.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "jh.mock")
public class MockProperties {

    /**
     * 是否模拟环境
     */
    private Boolean enabled = false;

    /**
     * 短信模拟环境的通知URL
     */
    private String smsNotifyUrl;
}
