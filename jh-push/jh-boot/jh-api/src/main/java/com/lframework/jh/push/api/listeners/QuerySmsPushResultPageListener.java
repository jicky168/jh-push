package com.lframework.jh.push.api.listeners;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lframework.jh.push.api.utils.SmsChannelUtil;
import com.lframework.jh.push.core.components.mq.MqProducer;
import com.lframework.jh.push.core.constants.MqConstants;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.plugin.core.SmsPushChannel;
import com.lframework.jh.push.plugin.core.dto.NotifySmsBatchDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultPageDto;
import com.lframework.jh.push.plugin.core.dto.QuerySmsPushResultPageRespDto;
import com.lframework.jh.push.service.entity.PushOrder;
import com.lframework.jh.push.service.entity.SmsBatchDetail;
import com.lframework.jh.push.service.enums.PushOrderStatus;
import com.lframework.jh.push.service.enums.SmsBatchDetailStatus;
import com.lframework.jh.push.service.service.PushOrderService;
import com.lframework.jh.push.service.service.SmsBatchDetailService;
import java.time.LocalDateTime;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 先不实现
 */
@Slf4j
@Component
public class QuerySmsPushResultPageListener {

    @Autowired
    private MqProducer mqProducer;

    @Autowired
    private SmsBatchDetailService smsBatchDetailService;

    @Autowired
    private PushOrderService pushOrderService;

    @JmsListener(destination = MqConstants.QUEUE_QUERY_SMS_PUSH_RESULT_PAGE)
    public void execute(QuerySmsPushResultPageDto dto) throws Exception {
        try {
            log.info("【第二步】（分页方式）接收到异步查询短信推送结果请求 dto {}", dto);

            SmsPushChannel channel = SmsChannelUtil.getByChannel(dto.getChannel());
            QuerySmsPushResultPageRespDto respDto = channel.query(dto);
            log.info("【第二步】（分页方式）异步查询短信通知推送结果 resp {}", respDto);
            if (respDto == null || CollectionUtil.isEmpty(respDto.getDetails())) {
                // 没有分页数据了，去统一回调
                this.doNotifyDto(dto);
                return;
            }
            for (QuerySmsPushResultPageRespDto.DetailDto resp : respDto.getDetails()) {
                if (resp.getStatus() == 2) {
                    Wrapper<SmsBatchDetail> updateWrapper = Wrappers.lambdaUpdate(
                            SmsBatchDetail.class)
                        .eq(SmsBatchDetail::getBatchNo, dto.getBatchNo())
                        .eq(SmsBatchDetail::getPhoneNumber, resp.getPhoneNumber())
                        .eq(SmsBatchDetail::getStatus, SmsBatchDetailStatus.CREATED)
                        .set(SmsBatchDetail::getErrorMsg, resp.getErrorMsg())
                        .set(SmsBatchDetail::getStatus, SmsBatchDetailStatus.FAIL);
                    smsBatchDetailService.update(updateWrapper);
                } else if (resp.getStatus() == 3) {
                    Wrapper<SmsBatchDetail> updateWrapper = Wrappers.lambdaUpdate(
                            SmsBatchDetail.class)
                        .eq(SmsBatchDetail::getBatchNo, dto.getBatchNo())
                        .eq(SmsBatchDetail::getPhoneNumber, resp.getPhoneNumber())
                        .eq(SmsBatchDetail::getStatus, SmsBatchDetailStatus.CREATED)
                        .set(SmsBatchDetail::getStatus, SmsBatchDetailStatus.SUCCESS)
                        .set(SmsBatchDetail::getSuccessTime, resp.getSuccessTime())
                        .set(SmsBatchDetail::getSmsContent, resp.getContent());
                    smsBatchDetailService.update(updateWrapper);
                } else {
                    // 只要当前页有尚未明确状态的批次明细，那么就重新查询
                    this.doRetryForRefresh(dto, false);
                    return;
                }
            }

            if (respDto.isHasNext()) {
                this.doRetryForRefresh(dto, true);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            this.doRetry(dto);
        }
    }

    private void doRetry(QuerySmsPushResultPageDto dto) {
        if (dto.getCurIndex() < MqConstants.SMS_ASYNC_QUERY_RETRY_SECONDS.length - 1) {
            log.info("【第二步】（分页方式）重试异步查询短信推送结果 dto {}", dto);
            dto.setCurIndex(dto.getCurIndex() + 1);
            mqProducer.sendDelayMessage(MqConstants.QUEUE_QUERY_SMS_PUSH_RESULT_PAGE, dto,
                MqConstants.SMS_ASYNC_QUERY_RETRY_SECONDS[dto.getCurIndex()] * 1000L);
        } else {
            log.info("【第二步】（分页方式）不再重试异步查询短信推送结果，原因：重试次数过多。dto {}",
                dto);
        }
    }

    private void doRetryForRefresh(QuerySmsPushResultPageDto dto, boolean isNextPage) {
        log.info("【第二步】（分页方式）有尚未明确状态的批次明细，重新分页查询 dto {}", dto);
        dto.setPageIndex(isNextPage ? dto.getPageIndex() + 1 : dto.getPageIndex());
        mqProducer.sendDelayMessage(MqConstants.QUEUE_QUERY_SMS_PUSH_RESULT_PAGE, dto,
            isNextPage ? 1 * 1000L : 10 * 1000L);
    }

    private void doNotifyDto(QuerySmsPushResultPageDto dto) {
        // 这里先查一下是否还有未查批次结果的明细记录
        Wrapper<SmsBatchDetail> queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
            .eq(SmsBatchDetail::getBatchNo, dto.getBatchNo())
            .eq(SmsBatchDetail::getStatus, SmsBatchDetailStatus.CREATED).last("LIMIT 1");
        SmsBatchDetail smsBatchDetail = smsBatchDetailService.getOne(queryWrapper);
        if (smsBatchDetail == null) {
            //所有的批次结果都查询过了
            PushOrder record = new PushOrder();
            record.setId(dto.getOrderId());
            record.setStatus(PushOrderStatus.SUCCESS);
            record.setSuccessTime(LocalDateTime.now());
            pushOrderService.updateById(record);
        }

        // 查询所有批次
        queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
            .eq(SmsBatchDetail::getBatchNo, dto.getBatchNo())
            .ne(SmsBatchDetail::getStatus, SmsBatchDetailStatus.CREATED);
        List<SmsBatchDetail> batchDetailList = smsBatchDetailService.list(queryWrapper);
        for (SmsBatchDetail batchDetail : batchDetailList) {
            NotifySmsBatchDto notifySmsBatchDto = new NotifySmsBatchDto();
            notifySmsBatchDto.setOrderId(dto.getOrderId());
            notifySmsBatchDto.setBatchNo(dto.getBatchNo());
            notifySmsBatchDto.setPhoneNumber(batchDetail.getPhoneNumber());
            notifySmsBatchDto.setSuccess(batchDetail.getStatus() == SmsBatchDetailStatus.SUCCESS);

            mqProducer.sendMessage(MqConstants.QUEUE_NOTIFY_SMS_BATCH, notifySmsBatchDto);
        }
    }
}
