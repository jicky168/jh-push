package com.lframework.jh.push.api.dto;

import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.plugin.core.SmsPushChannelParams;
import com.lframework.jh.push.plugin.core.enums.SmsQueryType;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QuerySmsBatchDetailDto implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 推送单ID
     */
    private String orderId;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 业务ID
     */
    private String bizId;

    /**
     * 配置参数
     */
    private SmsPushChannelParams params;

    /**
     * 手机号
     */
    private String phoneNumbers;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 推送时间
     */
    private LocalDateTime pushTime;

    /**
     * 查询方式
     */
    private SmsQueryType queryType;
}
