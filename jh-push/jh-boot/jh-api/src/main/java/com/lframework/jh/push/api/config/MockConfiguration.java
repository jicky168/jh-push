package com.lframework.jh.push.api.config;

import com.lframework.jh.push.api.config.properties.MockProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@EnableConfigurationProperties(MockProperties.class)
public class MockConfiguration {
}
