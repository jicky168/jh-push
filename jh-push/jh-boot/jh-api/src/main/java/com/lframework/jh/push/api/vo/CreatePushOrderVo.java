package com.lframework.jh.push.api.vo;

import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreatePushOrderVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商户ID
     */
    @ApiModelProperty(value = "商户ID", hidden = true)
    @NotNull(message = "商户ID不能为空！")
    private Integer mchId;

    /**
     * 应用ID
     */
    @ApiModelProperty(value = "应用ID")
    @NotBlank(message = "应用ID不能为空！")
    private String appId;

    /**
     * 渠道
     */
    @ApiModelProperty("渠道")
    @NotBlank(message = "渠道不能为空！")
    private String channel;

    /**
     * 外部单号
     */
    @ApiModelProperty("外部单号")
    @NotBlank(message = "外部单号不能为空！")
    private String outerNo;

    /**
     * 接收手机号
     */
    @ApiModelProperty(value = "接收手机号，手机号码之间以半角逗号（,）分隔。", example = "13700000000,13900000000", required = true)
    @NotBlank(message = "接收手机号不能为空！")
    private String phoneNumbers;

    /**
     * 通知URL
     */
    @ApiModelProperty("通知URL")
    @NotBlank(message = "通知URL不能为空！")
    private String notifyUrl;

    /**
     * 附加参数1
     */
    @ApiModelProperty("附加参数1")
    private String param1;

    /**
     * 附加参数2
     */
    @ApiModelProperty("附加参数2")
    private String param2;

    /**
     * 扩展参数
     */
    @ApiModelProperty("扩展参数")
    private String extra;
}
