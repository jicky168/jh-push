package com.lframework.jh.push.api.handlers;

import com.lframework.jh.push.core.components.sign.CheckSignHandler;
import com.lframework.jh.push.core.utils.StringUtil;
import com.lframework.jh.push.core.vo.OpenApiReqVo;
import com.lframework.jh.push.service.entity.Mch;
import com.lframework.jh.push.service.service.AppService;
import com.lframework.jh.push.service.service.MchService;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApiCheckSignHandler implements CheckSignHandler {

    @Autowired
    private AppService appService;

    @Autowired
    private MchService mchService;

    @Override
    public boolean check(OpenApiReqVo req) {
        Integer mchId = req.getMchId();
        if (mchId == null) {
            return false;
        }

        String timestamp = req.getTimestamp();
        if (StringUtil.isBlank(timestamp)) {
            return false;
        }

        String nonceStr = req.getNonceStr();
        if (StringUtil.isBlank(nonceStr)) {
            return false;
        }

        String params = req.getParams();
        if (StringUtil.isBlank(params)) {
            return false;
        }

        String sign = req.getSign();
        if (StringUtil.isBlank(params)) {
            return false;
        }

        Mch mch = mchService.findById(mchId);
        if (mch == null || !mch.getAvailable() || StringUtil.isBlank(mch.getApiSecret())) {
            return false;
        }

        return this
            .validate(mchId, mch.getApiSecret(), timestamp, nonceStr, params, sign);
    }

    @Override
    public String sign(Integer mchId, String timestamp, String nonceStr, String json) {
        Mch mch = mchService.findById(mchId);
        return this.doSign(mchId, mch.getApiSecret(), timestamp, nonceStr, json);
    }

    @Override
    public String sign(Integer mchId, String apiSecret, String timestamp, String nonceStr,
        String json) {
        return this.doSign(mchId, apiSecret, timestamp, nonceStr, json);
    }

    /**
     * 加签
     *
     * @param mchId     商户ID
     * @param apiSecret 通信密钥
     * @param timestamp 时间戳
     * @param nonceStr  随机字符串
     * @param json      请求参数
     * @return
     */
    private String doSign(Integer mchId, String apiSecret, String timestamp, String nonceStr,
        String json) {
        String str = new StringBuilder().append("mchId=").append(mchId).append("&")
            .append("apiSecret=").append(apiSecret)
            .append("&").append("timestamp=").append(timestamp).append("&").append("nonceStr=")
            .append(nonceStr).append("&").append("params=")
            .append(json).toString();
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
        byte[] bs = digest.digest(str.getBytes(StandardCharsets.UTF_8));
        StringBuilder builder = new StringBuilder();
        for (byte b : bs) {
            int x = b & 255;
            String s = Integer.toHexString(x);
            if (x > 0 && x < 16) {
                builder.append("0");
                builder.append(s);
            } else {
                builder.append(s);
            }
        }

        return builder.toString();
    }

    /**
     * 验签
     *
     * @param mchId     商户ID
     * @param apiSecret 通信密钥
     * @param timestamp 时间戳
     * @param nonceStr  随机字符串
     * @param json      请求参数
     * @param oriSign   原始签值
     * @return
     */
    private boolean validate(Integer mchId, String apiSecret, String timestamp,
        String nonceStr, String json,
        String oriSign) {
        return sign(mchId, apiSecret, timestamp, nonceStr, json).equalsIgnoreCase(oriSign);
    }
}
