package com.lframework.jh.push.api.utils;

import com.lframework.jh.push.api.config.properties.MockProperties;
import com.lframework.jh.push.core.utils.ApplicationUtil;
import com.lframework.jh.push.plugin.core.SmsPushChannel;
import com.lframework.jh.push.plugin.core.constants.PushConstants;

import java.util.Locale;

public class SmsChannelUtil {

    public static SmsPushChannel getByChannel(String channel) {

        MockProperties mockProperties = ApplicationUtil.getBean(MockProperties.class);

        return (SmsPushChannel) ApplicationUtil.getBean(channel.toLowerCase(Locale.ROOT) + (mockProperties.getEnabled() ? PushConstants.MOCK_SMS_PUSH_CHANNEL_BEAN_NAME_SUFFIX : PushConstants.SMS_PUSH_CHANNEL_BEAN_NAME_SUFFIX));
    }
}
