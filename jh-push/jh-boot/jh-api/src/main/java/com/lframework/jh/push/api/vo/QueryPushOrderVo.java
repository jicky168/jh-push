package com.lframework.jh.push.api.vo;

import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class QueryPushOrderVo implements BaseVo, Serializable {

    /**
     * 应用ID
     */
    @ApiModelProperty(value = "应用ID")
    @NotBlank(message = "应用ID不能为空！")
    private String appId;

    /**
     * 推送单ID
     */
    @ApiModelProperty("推送单ID")
    private String orderId;

    /**
     * 外部单号
     */
    @ApiModelProperty("外部单号")
    private String outerNo;

    /**
     * 商户ID
     */
    @ApiModelProperty(value = "商户ID", hidden = true)
    @NotNull(message = "商户ID不能为空！")
    private Integer mchId;
}
