package com.lframework.jh.push.api.bo;

import com.lframework.jh.push.core.annotations.convert.EnumConvert;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.utils.JsonUtil;
import com.lframework.jh.push.core.utils.StringUtil;
import com.lframework.jh.push.service.entity.PushOrder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;

@Data
public class PushSmsBo extends BaseBo<PushOrder> {

    /**
     * ID
     */
    @ApiModelProperty(value = "推送单ID", example = "P1688569238278770688")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty(value = "商户ID", example = "10000")
    private Integer mchId;

    /**
     * 应用ID
     */
    @ApiModelProperty(value = "应用ID", example = "536233e9cbb9496a80b13e85ee34d467")
    private String appId;

    /**
     * 渠道
     */
    @ApiModelProperty(value = "渠道", example = "ALI_SMS")
    private String channel;

    /**
     * 接收手机号
     */
    @ApiModelProperty(value = "接收手机号", example = "17600000000,17600000001")
    private String phoneNumbers;

    /**
     * 外部单号
     */
    @ApiModelProperty(value = "外部单号", example = "1688569211214221312")
    private String outerNo;

    /**
     * 业务ID
     */
    @ApiModelProperty(value = "业务ID", example = "53e05a334885433a83e80d7cbb7fd06f")
    private String bizId;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态 0-待推送 3-推送中 6-推送完成 9-推送失败", example = "0")
    @EnumConvert
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", example = "2023-01-01 00:00:00")
    private LocalDateTime createTime;

    /**
     * 渠道响应
     */
    @ApiModelProperty(value = "渠道响应", example = "{\"bizId\":\"53e05a334885433a83e80d7cbb7fd06f\",\"requestId\":\"847c7f10988047cb8819fa59b2135964\"}")
    private Map<String, Object> channelResp;

    /**
     * 成功时间
     */
    @ApiModelProperty(value = "成功时间", example = "2023-01-01 00:00:01")
    private LocalDateTime successTime;

    /**
     * 失败时间
     */
    @ApiModelProperty(value = "失败时间", example = "2023-01-01 00:00:02")
    private LocalDateTime failureTime;

    /**
     * 错误原因
     */
    @ApiModelProperty(value = "错误原因")
    private String errorMsg;

    /**
     * 批次号
     */
    @ApiModelProperty(value = "批次号", example = "B1688570231754199040")
    private String batchNo;

    public PushSmsBo() {
    }

    public PushSmsBo(PushOrder dto) {
        super(dto);
    }

    @Override
    public <A> BaseBo<PushOrder> convert(PushOrder dto) {
        return super.convert(dto, PushSmsBo::getChannelResp);
    }

    @Override
    protected void afterInit(PushOrder dto) {
        if (StringUtil.isNotBlank(dto.getChannelResp())) {
            this.channelResp = JsonUtil.parseObj(dto.getChannelResp());
        } else {
            this.channelResp = null;
        }
    }
}
