package com.lframework.jh.push.api.controller;

import com.lframework.jh.push.core.annotations.OpenApi;
import com.lframework.jh.push.core.components.sign.CheckSignFactory;
import com.lframework.jh.push.core.controller.DefaultBaseController;
import com.lframework.jh.push.core.utils.JsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = "模拟接收通知", hidden = true)
@Slf4j
@RestController
@RequestMapping("/mock/notify")
public class MockReceiveNotifyController extends DefaultBaseController {

    @Autowired
    private CheckSignFactory checkSignFactory;

    @ApiOperation("接收短信通知")
    @OpenApi
    @PostMapping("/sms")
    public String testNotify(@RequestBody String str) {
        log.info("接收到测试通知 {}", str);

        Map<String, String> map = JsonUtil.parseMap(str, String.class, String.class);
        log.info("map = {}", map);

        String sign = checkSignFactory.getInstance().sign(Integer.valueOf(map.get("mchId")), map.get("timestamp"), map.get("nonceStr"), map.get("params"));

        return sign.equals(map.get("sign")) ? "SUCCESS" : "FAIL";
    }
}
