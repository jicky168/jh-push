package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.PushOrder;

public interface PushOrderMapper extends BaseMpMapper<PushOrder> {
}
