package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.App;
import com.lframework.jh.push.service.vo.app.AppSelectorVo;
import com.lframework.jh.push.service.vo.app.CreateAppVo;
import com.lframework.jh.push.service.vo.app.QueryAppVo;
import com.lframework.jh.push.service.vo.app.UpdateAppVo;

import java.util.List;

public interface AppService extends BaseMpService<App> {

    /**
     * 查询列表
     *
     * @return
     */
    PageResult<App> query(Integer pageIndex, Integer pageSize, QueryAppVo vo);

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<App> query(QueryAppVo vo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    App findById(String id);

    /**
     * 选择器
     *
     * @return
     */
    PageResult<App> selector(Integer pageIndex, Integer pageSize, AppSelectorVo vo);

    /**
     * 创建
     *
     * @param vo
     * @return
     */
    String create(CreateAppVo vo);

    /**
     * 修改
     *
     * @param vo
     */
    void update(UpdateAppVo vo);
}
