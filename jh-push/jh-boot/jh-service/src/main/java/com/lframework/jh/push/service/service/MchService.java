package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.Mch;
import com.lframework.jh.push.service.vo.mch.CreateMchVo;
import com.lframework.jh.push.service.vo.mch.MchSelectorVo;
import com.lframework.jh.push.service.vo.mch.QueryMchVo;
import com.lframework.jh.push.service.vo.mch.UpdateMchVo;
import java.util.List;

public interface MchService extends BaseMpService<Mch> {

    /**
     * 查询列表
     *
     * @return
     */
    PageResult<Mch> query(Integer pageIndex, Integer pageSize, QueryMchVo vo);

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<Mch> query(QueryMchVo vo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Mch findById(Integer id);

    /**
     * 选择器
     *
     * @return
     */
    PageResult<Mch> selector(Integer pageIndex, Integer pageSize, MchSelectorVo vo);

    /**
     * 创建
     *
     * @param vo
     * @return
     */
    Integer create(CreateMchVo vo);

    /**
     * 修改
     *
     * @param vo
     */
    void update(UpdateMchVo vo);
}
