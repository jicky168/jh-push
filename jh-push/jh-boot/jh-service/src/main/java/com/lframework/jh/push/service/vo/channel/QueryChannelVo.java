package com.lframework.jh.push.service.vo.channel;

import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class QueryChannelVo extends PageVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty("编号")
    private String code;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 创建起始时间
     */
    @ApiModelProperty("创建起始时间")
    private LocalDateTime startTime;

    /**
     * 创建截止时间
     */
    @ApiModelProperty("创建截止时间")
    private LocalDateTime endTime;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;
}
