package com.lframework.jh.push.service.listeners;

import com.lframework.jh.push.core.components.security.AbstractUserDetails;
import com.lframework.jh.push.core.utils.SecurityUtil;
import com.lframework.jh.push.service.enums.OpLogType;
import com.lframework.jh.push.service.events.LoginEvent;
import com.lframework.jh.push.service.utils.OpLogUtil;
import com.lframework.jh.push.service.vo.log.CreateOpLogsVo;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class LoginListener implements ApplicationListener<LoginEvent> {

    @Override
    public void onApplicationEvent(LoginEvent loginEvent) {

        AbstractUserDetails currentUser = SecurityUtil.getCurrentUser();
        CreateOpLogsVo vo = new CreateOpLogsVo();
        vo.setName("用户登录");
        vo.setLogType(OpLogType.AUTH);
        vo.setCreateBy(currentUser.getName());
        vo.setCreateById(currentUser.getId());
        vo.setIp(currentUser.getIp());

        OpLogUtil.addLog(vo);
    }
}
