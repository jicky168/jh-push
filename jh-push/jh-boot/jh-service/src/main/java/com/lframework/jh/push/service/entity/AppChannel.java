package com.lframework.jh.push.service.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.core.entity.BaseEntity;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("tbl_app_channel")
public class AppChannel extends BaseEntity implements BaseDto, Serializable {
    private static final long serialVersionUID = 1L;

    public static final String CACHE_NAME = "AppChannel";

    /**
     * ID
     */
    private String id;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 渠道ID
     */
    private String channelId;

    /**
     * 配置参数
     */
    private String params;

    /**
     * 状态
     */
    private Boolean available;
}
