package com.lframework.jh.push.service.listeners;

import com.lframework.jh.push.service.enums.OpLogType;
import com.lframework.jh.push.service.events.LogoutEvent;
import com.lframework.jh.push.service.utils.OpLogUtil;
import com.lframework.jh.push.service.vo.log.CreateOpLogsVo;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class LogoutListener implements ApplicationListener<LogoutEvent> {

    @Override
    public void onApplicationEvent(LogoutEvent event) {

        CreateOpLogsVo vo = new CreateOpLogsVo();
        vo.setName("退出登录");
        vo.setLogType(OpLogType.AUTH);
        vo.setCreateBy(event.getUser().getName());
        vo.setCreateById(event.getUser().getId());
        vo.setIp(event.getUser().getIp());

        OpLogUtil.addLog(vo);
    }
}
