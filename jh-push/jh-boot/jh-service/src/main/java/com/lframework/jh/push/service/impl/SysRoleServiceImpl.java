package com.lframework.jh.push.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.components.security.SecurityConstants;
import com.lframework.jh.push.core.constants.StringPool;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.impl.BaseMpServiceImpl;
import com.lframework.jh.push.core.utils.*;
import com.lframework.jh.push.service.annotations.OpLog;
import com.lframework.jh.push.service.entity.SysRole;
import com.lframework.jh.push.service.enums.OpLogType;
import com.lframework.jh.push.service.mappers.SysRoleMapper;
import com.lframework.jh.push.service.service.SysMenuService;
import com.lframework.jh.push.service.service.SysRoleService;
import com.lframework.jh.push.service.utils.OpLogUtil;
import com.lframework.jh.push.service.vo.role.CreateSysRoleVo;
import com.lframework.jh.push.service.vo.role.QuerySysRoleVo;
import com.lframework.jh.push.service.vo.role.SysRoleSelectorVo;
import com.lframework.jh.push.service.vo.role.UpdateSysRoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Service
public class SysRoleServiceImpl extends BaseMpServiceImpl<SysRoleMapper, SysRole> implements
        SysRoleService {

    @Autowired
    private SysMenuService sysMenuService;

    @Override
    public PageResult<SysRole> query(Integer pageIndex, Integer pageSize,
                                     QuerySysRoleVo vo) {

        Assert.greaterThanZero(pageIndex);
        Assert.greaterThanZero(pageSize);

        PageHelperUtil.startPage(pageIndex, pageSize);
        List<SysRole> datas = this.query(vo);

        return PageResultUtil.convert(new PageInfo<>(datas));
    }

    @Override
    public List<SysRole> query(QuerySysRoleVo vo) {

        return this.doQuery(vo);
    }

    @Cacheable(value = SysRole.CACHE_NAME, key = "#id", unless = "#result == null")
    @Override
    public SysRole findById(String id) {

        return this.doGetById(id);
    }

    @Override
    public PageResult<SysRole> selector(Integer pageIndex, Integer pageSize,
                                        SysRoleSelectorVo vo) {

        Assert.greaterThanZero(pageIndex);
        Assert.greaterThanZero(pageSize);

        PageHelperUtil.startPage(pageIndex, pageSize);
        List<SysRole> datas = this.doSelector(vo);

        return PageResultUtil.convert(new PageInfo<>(datas));
    }

    @OpLog(type = OpLogType.SYS, name = "停用角色，ID：{}", params = "#ids", loopFormat = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchUnable(Collection<String> ids) {

        if (CollectionUtil.isEmpty(ids)) {
            return;
        }

        for (String id : ids) {
            SysRole role = this.findById(id);
            if (SecurityConstants.PERMISSION_ADMIN_NAME.equals(role.getPermission())) {
                throw new DefaultClientException(
                        "角色【" + role.getName() + "】的权限为【" + SecurityConstants.PERMISSION_ADMIN_NAME
                                + "】，不允许停用！");
            }
        }

        this.doBatchUnable(ids);
    }

    @OpLog(type = OpLogType.SYS, name = "启用角色，ID：{}", params = "#ids", loopFormat = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void batchEnable(Collection<String> ids) {

        if (CollectionUtil.isEmpty(ids)) {
            return;
        }

        for (String id : ids) {
            SysRole role = this.findById(id);
            if (SecurityConstants.PERMISSION_ADMIN_NAME.equals(role.getPermission())) {
                throw new DefaultClientException(
                        "角色【" + role.getName() + "】的权限为【" + SecurityConstants.PERMISSION_ADMIN_NAME
                                + "】，不允许启用！");
            }
        }

        this.doBatchEnable(ids);
    }

    @OpLog(type = OpLogType.SYS, name = "新增角色，ID：{}, 编号：{}", params = {"#id", "#code"})
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String create(CreateSysRoleVo vo) {

        if (!StringUtil.isBlank(vo.getPermission())) {

            if (SecurityConstants.PERMISSION_ADMIN_NAME.equals(vo.getPermission())) {
                throw new DefaultClientException(
                        "权限【" + SecurityConstants.PERMISSION_ADMIN_NAME + "】为内置权限，请修改！");
            }

            // 这里的权限不能与菜单权限重复
            if (sysMenuService.existPermission(vo.getPermission())) {
                throw new DefaultClientException(
                        "权限【" + vo.getPermission() + "】为菜单权限，请修改！");
            }
        }

        SysRole data = this.doCreate(vo);

        OpLogUtil.setVariable("id", data.getId());
        OpLogUtil.setVariable("code", vo.getCode());
        OpLogUtil.setExtra(vo);

        return data.getId();
    }

    @OpLog(type = OpLogType.SYS, name = "修改角色，ID：{}, 编号：{}", params = {"#id", "#code"})
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(UpdateSysRoleVo vo) {

        SysRole data = this.findById(vo.getId());
        if (ObjectUtil.isNull(data)) {
            throw new DefaultClientException("角色不存在！");
        }

        if (SecurityConstants.PERMISSION_ADMIN_NAME.equals(data.getPermission())) {
            throw new DefaultClientException("角色【" + data.getName() + "】为内置角色，不允许修改！");
        }

        if (!StringUtil.isBlank(vo.getPermission())) {

            if (SecurityConstants.PERMISSION_ADMIN_NAME.equals(vo.getPermission())) {
                throw new DefaultClientException(
                        "权限【" + SecurityConstants.PERMISSION_ADMIN_NAME + "】为内置权限，请修改！");
            }

            // 这里的权限不能与菜单权限重复
            if (sysMenuService.existPermission(vo.getPermission())) {
                throw new DefaultClientException(
                        "权限【" + vo.getPermission() + "】为菜单权限，请修改！");
            }
        }

        this.doUpdate(vo);

        OpLogUtil.setVariable("id", data.getId());
        OpLogUtil.setVariable("code", vo.getCode());
        OpLogUtil.setExtra(vo);
    }

    @Override
    public List<SysRole> getByUserId(String userId) {

        return this.doGetByUserId(userId);
    }

    protected List<SysRole> doQuery(QuerySysRoleVo vo) {

        return getBaseMapper().query(vo);
    }

    protected SysRole doGetById(String id) {

        return getBaseMapper().findById(id);
    }

    protected List<SysRole> doSelector(SysRoleSelectorVo vo) {

        return getBaseMapper().selector(vo);
    }

    protected void doBatchUnable(Collection<String> ids) {

        Wrapper<SysRole> updateWrapper = Wrappers.lambdaUpdate(SysRole.class)
                .set(SysRole::getAvailable, Boolean.FALSE).in(SysRole::getId, ids);
        getBaseMapper().update(updateWrapper);
    }

    protected void doBatchEnable(Collection<String> ids) {

        Wrapper<SysRole> updateWrapper = Wrappers.lambdaUpdate(SysRole.class)
                .set(SysRole::getAvailable, Boolean.TRUE).in(SysRole::getId, ids);
        getBaseMapper().update(updateWrapper);
    }

    protected SysRole doCreate(CreateSysRoleVo vo) {

        Wrapper<SysRole> checkWrapper = Wrappers.lambdaQuery(SysRole.class)
                .eq(SysRole::getCode, vo.getCode());
        if (getBaseMapper().selectCount(checkWrapper) > 0) {
            throw new DefaultClientException("编号重复，请重新输入！");
        }

        checkWrapper = Wrappers.lambdaQuery(SysRole.class)
                .eq(SysRole::getName, vo.getName());
        if (getBaseMapper().selectCount(checkWrapper) > 0) {
            throw new DefaultClientException("名称重复，请重新输入！");
        }

        SysRole data = new SysRole();
        data.setId(IdUtil.getId());
        data.setCode(vo.getCode());
        data.setName(vo.getName());

        if (!StringUtil.isBlank(vo.getPermission())) {

            data.setPermission(vo.getPermission());
        }

        data.setAvailable(Boolean.TRUE);
        data.setDescription(
                StringUtil.isBlank(vo.getDescription()) ? StringPool.EMPTY_STR : vo.getDescription());

        getBaseMapper().insert(data);

        return data;
    }

    protected void doUpdate(UpdateSysRoleVo vo) {

        Wrapper<SysRole> checkWrapper = Wrappers.lambdaQuery(SysRole.class)
                .eq(SysRole::getCode, vo.getCode()).ne(SysRole::getId, vo.getId());
        if (getBaseMapper().selectCount(checkWrapper) > 0) {
            throw new DefaultClientException("编号重复，请重新输入！");
        }

        checkWrapper = Wrappers.lambdaQuery(SysRole.class)
                .eq(SysRole::getName, vo.getName())
                .ne(SysRole::getId, vo.getId());
        if (getBaseMapper().selectCount(checkWrapper) > 0) {
            throw new DefaultClientException("名称重复，请重新输入！");
        }

        LambdaUpdateWrapper<SysRole> updateWrapper = Wrappers.lambdaUpdate(SysRole.class)
                .set(SysRole::getCode, vo.getCode()).set(SysRole::getName, vo.getName())
                .set(SysRole::getPermission, null)
                .set(SysRole::getAvailable, vo.getAvailable())
                .set(SysRole::getDescription,
                        StringUtil.isBlank(vo.getDescription()) ? StringPool.EMPTY_STR : vo.getDescription())
                .eq(SysRole::getId, vo.getId());

        if (!StringUtil.isBlank(vo.getPermission())) {

            updateWrapper.set(SysRole::getPermission, vo.getPermission());
        }

        getBaseMapper().update(updateWrapper);
    }

    protected List<SysRole> doGetByUserId(String userId) {

        return getBaseMapper().getByUserId(userId);
    }

    @CacheEvict(value = SysRole.CACHE_NAME, key = "#key")
    @Override
    public void cleanCacheByKey(Serializable key) {

    }
}
