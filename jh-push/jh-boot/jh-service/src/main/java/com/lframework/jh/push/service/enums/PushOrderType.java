package com.lframework.jh.push.service.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.lframework.jh.push.core.enums.BaseEnum;

public enum PushOrderType implements BaseEnum<Integer> {
    SMS(0, "短信");

    @EnumValue
    private final Integer code;

    private final String desc;

    PushOrderType(Integer code, String desc) {

        this.code = code;
        this.desc = desc;
    }

    @Override
    public Integer getCode() {

        return this.code;
    }

    @Override
    public String getDesc() {

        return this.desc;
    }
}
