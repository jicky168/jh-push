package com.lframework.jh.push.service.impl;

import com.lframework.jh.push.core.impl.BaseMpServiceImpl;
import com.lframework.jh.push.service.entity.PushOrder;
import com.lframework.jh.push.service.mappers.PushOrderMapper;
import com.lframework.jh.push.service.service.PushOrderService;
import org.springframework.stereotype.Service;

@Service
public class PushOrderServiceImpl extends BaseMpServiceImpl<PushOrderMapper, PushOrder> implements PushOrderService {
}
