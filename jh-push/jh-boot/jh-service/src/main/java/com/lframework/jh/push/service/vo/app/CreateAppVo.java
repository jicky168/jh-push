package com.lframework.jh.push.service.vo.app;

import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateAppVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    @NotBlank(message = "请输入名称！")
    private String name;

    /**
     * 商户ID
     */
    @ApiModelProperty(value = "商户ID", required = true)
    @NotNull(message = "商户ID不能为空！")
    private Integer mchId;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;
}
