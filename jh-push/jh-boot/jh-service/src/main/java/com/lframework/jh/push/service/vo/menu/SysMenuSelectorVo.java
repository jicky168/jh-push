package com.lframework.jh.push.service.vo.menu;

import com.lframework.jh.push.core.components.validation.IsEnum;
import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.service.enums.SysMenuDisplay;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysMenuSelectorVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("类型")
    @IsEnum(message = "类型格式不正确！", enumClass = SysMenuDisplay.class)
    private Integer display;
}
