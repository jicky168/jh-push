package com.lframework.jh.push.service.vo.mch;

import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CreateMchVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    @NotBlank(message = "请输入名称！")
    private String name;

    /**
     * 通信密钥
     */
    @ApiModelProperty(value = "通信密钥")
    private String apiSecret;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;
}
