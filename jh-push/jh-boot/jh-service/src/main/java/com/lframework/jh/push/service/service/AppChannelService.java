package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.AppChannel;

public interface AppChannelService extends BaseMpService<AppChannel> {

    /**
     * 根据应用ID、渠道ID查询
     *
     * @param appId 应用ID
     * @param channelId 渠道ID
     * @return
     */
    AppChannel findByAppIdAndChannelId(String appId, String channelId);
}
