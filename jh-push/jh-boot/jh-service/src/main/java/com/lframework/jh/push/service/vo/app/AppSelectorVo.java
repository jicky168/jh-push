package com.lframework.jh.push.service.vo.app;

import com.lframework.jh.push.core.components.validation.TypeMismatch;
import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@Data
public class AppSelectorVo extends PageVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty(value = "商户ID")
    @TypeMismatch(message = "商户ID格式错误！")
    private Integer mchId;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;
}
