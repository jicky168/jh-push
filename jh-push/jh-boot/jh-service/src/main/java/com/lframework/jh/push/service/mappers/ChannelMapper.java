package com.lframework.jh.push.service.mappers;


import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.Channel;
import com.lframework.jh.push.service.vo.channel.ChannelSelectorVo;
import com.lframework.jh.push.service.vo.channel.QueryChannelVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-07-04
 */
public interface ChannelMapper extends BaseMpMapper<Channel> {

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<Channel> query(@Param("vo") QueryChannelVo vo);

    /**
     * 选择器
     *
     * @param vo
     * @return
     */
    List<Channel> selector(@Param("vo") ChannelSelectorVo vo);
}
