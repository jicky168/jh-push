package com.lframework.jh.push.service.service;


import com.lframework.jh.push.core.components.security.AbstractUserDetails;
import com.lframework.jh.push.core.exceptions.impl.UserLoginException;

public interface UserDetailsService {

    /**
     * 根据用户名查询 用于登录认证
     *
     * @param username
     * @return
     * @throws UserLoginException
     */
    AbstractUserDetails loadUserByUsername(String username) throws UserLoginException;
}
