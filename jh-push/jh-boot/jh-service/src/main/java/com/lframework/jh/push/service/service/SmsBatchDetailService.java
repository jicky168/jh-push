package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.SmsBatchDetail;

public interface SmsBatchDetailService extends BaseMpService<SmsBatchDetail> {

    /**
     * 根据批次号修改为失败状态
     * @param batchNo
     */
    void updateFailByBatchNo(String batchNo, String errorMsg);
}
