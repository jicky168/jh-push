package com.lframework.jh.push.service.vo.mch;

import com.lframework.jh.push.core.components.validation.TypeMismatch;
import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QueryMchVo extends PageVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty("ID")
    @TypeMismatch(message = "ID格式错误！")
    private Integer id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 创建起始时间
     */
    @ApiModelProperty("创建起始时间")
    private LocalDateTime startTime;

    /**
     * 创建截止时间
     */
    @ApiModelProperty("创建截止时间")
    private LocalDateTime endTime;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;
}
