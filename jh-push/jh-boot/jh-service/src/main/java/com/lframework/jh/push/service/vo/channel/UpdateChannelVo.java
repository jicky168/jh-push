package com.lframework.jh.push.service.vo.channel;

import com.lframework.jh.push.core.components.validation.IsCode;
import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class UpdateChannelVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty(value = "ID", required = true)
    @NotBlank(message = "ID不能为空！")
    private String id;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", required = true)
    @IsCode
    @NotBlank(message = "请输入编号！")
    private String code;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    @NotBlank(message = "请输入名称！")
    private String name;

    /**
     * 配置参数
     */
    @ApiModelProperty("配置参数")
    private String params;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", required = true)
    @NotNull(message = "状态不能为空！")
    private Boolean available;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;
}
