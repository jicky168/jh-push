package com.lframework.jh.push.service.impl;

import com.lframework.jh.push.core.components.security.AbstractUserDetails;
import com.lframework.jh.push.service.mappers.UserDetailsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * UserDetaisService默认实现
 *
 * @author zmj
 */
@Service
public class UserDetailsService extends AbstractUserDetailsService {

    @Autowired
    private UserDetailsMapper userDetailsMapper;

    @Override
    public AbstractUserDetails findByUsername(String username) {

        AbstractUserDetails user = userDetailsMapper.findByUsername(username);

        return user;
    }
}
