package com.lframework.jh.push.service.mappers;


import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.SysRoleMenu;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-07-04
 */
public interface SysRoleMenuMapper extends BaseMpMapper<SysRoleMenu> {

}
