package com.lframework.jh.push.service.service;


import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.SysRole;
import com.lframework.jh.push.service.vo.role.CreateSysRoleVo;
import com.lframework.jh.push.service.vo.role.QuerySysRoleVo;
import com.lframework.jh.push.service.vo.role.SysRoleSelectorVo;
import com.lframework.jh.push.service.vo.role.UpdateSysRoleVo;

import java.util.Collection;
import java.util.List;

public interface SysRoleService extends BaseMpService<SysRole> {

    /**
     * 查询列表
     *
     * @return
     */
    PageResult<SysRole> query(Integer pageIndex, Integer pageSize, QuerySysRoleVo vo);

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<SysRole> query(QuerySysRoleVo vo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    SysRole findById(String id);

    /**
     * 选择器
     *
     * @return
     */
    PageResult<SysRole> selector(Integer pageIndex, Integer pageSize, SysRoleSelectorVo vo);

    /**
     * 根据ID停用
     *
     * @param ids
     */
    void batchUnable(Collection<String> ids);

    /**
     * 根据ID启用
     *
     * @param ids
     */
    void batchEnable(Collection<String> ids);

    /**
     * 创建
     *
     * @param vo
     * @return
     */
    String create(CreateSysRoleVo vo);

    /**
     * 修改
     *
     * @param vo
     */
    void update(UpdateSysRoleVo vo);

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    List<SysRole> getByUserId(String userId);
}
