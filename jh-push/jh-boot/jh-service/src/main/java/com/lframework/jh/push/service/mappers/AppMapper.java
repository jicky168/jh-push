package com.lframework.jh.push.service.mappers;


import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.App;
import com.lframework.jh.push.service.vo.app.AppSelectorVo;
import com.lframework.jh.push.service.vo.app.QueryAppVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-07-04
 */
public interface AppMapper extends BaseMpMapper<App> {
    
    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<App> query(@Param("vo") QueryAppVo vo);

    /**
     * 选择器
     *
     * @param vo
     * @return
     */
    List<App> selector(@Param("vo") AppSelectorVo vo);
}
