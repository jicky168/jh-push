package com.lframework.jh.push.service.vo.app.channel;

import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SetParamsVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 应用ID
     */
    @ApiModelProperty(value = "应用ID", required = true)
    @NotBlank(message = "应用ID不能为空！")
    private String appId;

    /**
     * 渠道ID
     */
    @ApiModelProperty(value = "渠道ID", required = true)
    @NotBlank(message = "渠道ID不能为空！")
    private String channelId;

    /**
     * 参数
     */
    @ApiModelProperty(value = "参数", required = true)
    @NotBlank(message = "参数不能为空！")
    private String params = "{}";

    /**
     * 是否开通
     */
    @ApiModelProperty(value = "是否开通", required = true)
    @NotNull(message = "是否开通不能为空！")
    private Boolean available;
}
