package com.lframework.jh.push.service.service;

import com.lframework.jh.push.service.entity.NotifyData;
import com.lframework.jh.push.core.service.BaseMpService;

public interface NotifyDataService extends BaseMpService<NotifyData> {

    /**
     * 根据ID增加通知次数
     *
     * @param id
     * @param resp
     */
    void addNotifyCount(String id, String resp);
}
