package com.lframework.jh.push.service.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.lframework.jh.push.core.enums.BaseEnum;

public enum PushOrderStatus implements BaseEnum<Integer> {
    CREATED(0, "待推送"), PUSHING(3, "推送中"), SUCCESS(6, "推送完成"), FAIL(9, "推送失败");

    @EnumValue
    private final Integer code;

    private final String desc;

    PushOrderStatus(Integer code, String desc) {

        this.code = code;
        this.desc = desc;
    }

    @Override
    public Integer getCode() {

        return this.code;
    }

    @Override
    public String getDesc() {

        return this.desc;
    }
}
