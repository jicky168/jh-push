package com.lframework.jh.push.service.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.core.entity.BaseEntity;
import com.lframework.jh.push.service.enums.PushOrderStatus;
import com.lframework.jh.push.service.enums.PushOrderType;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("tbl_push_order")
public class PushOrder extends BaseEntity implements BaseDto, Serializable {
    private static final long serialVersionUID = 1L;

    public static final String CACHE_NAME = "PushOrder";

    /**
     * ID
     */
    private String id;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 渠道ID
     */
    private String channelId;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 接收手机号
     */
    private String phoneNumbers;

    /**
     * 外部单号
     */
    private String outerNo;

    /**
     * 渠道参数
     */
    private String channelParams;

    /**
     * 通知Url
     */
    private String notifyUrl;

    /**
     * 业务ID
     */
    private String bizId;

    /**
     * 附加参数1
     */
    private String param1;

    /**
     * 附加参数2
     */
    private String param2;

    /**
     * 扩展参数
     */
    private String extra;

    /**
     * 推送单类型
     */
    private PushOrderType orderType;

    /**
     * 状态
     */
    private PushOrderStatus status;

    /**
     * 创建时间 新增时赋值
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间 新增和修改时赋值
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 渠道响应数据
     */
    private String channelResp;

    /**
     * 成功时间
     */
    private LocalDateTime successTime;

    /**
     * 失败时间
     */
    private LocalDateTime failureTime;

    /**
     * 失败原因
     */
    private String errorMsg;

    /**
     * 批次号
     */
    private String batchNo;
}
