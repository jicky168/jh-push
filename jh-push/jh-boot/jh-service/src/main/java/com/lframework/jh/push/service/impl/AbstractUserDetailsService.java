package com.lframework.jh.push.service.impl;

import com.lframework.jh.push.core.components.security.AbstractUserDetails;
import com.lframework.jh.push.core.components.security.SecurityConstants;
import com.lframework.jh.push.core.exceptions.impl.UserLoginException;
import com.lframework.jh.push.core.utils.ObjectUtil;
import com.lframework.jh.push.core.utils.RequestUtil;
import com.lframework.jh.push.service.service.SysMenuService;
import com.lframework.jh.push.service.service.UserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Set;

/**
 * 所有UserDetailsService都需要继承此类 如果需要更改用户表，那么继承此类注册Bean即可
 *
 * @author zmj
 */
@Slf4j
public abstract class AbstractUserDetailsService implements UserDetailsService {

    @Autowired
    private SysMenuService sysMenuService;

    @Value("${remove-fixed-permissions:false}")
    private Boolean removeFixedPermission;

    @Override
    public AbstractUserDetails loadUserByUsername(String username) throws UserLoginException {

        //根据登录名查询
        AbstractUserDetails userDetails = this.findByUsername(username);

        if (ObjectUtil.isEmpty(userDetails)) {
            log.debug("用户名 {} 不存在", username);
            throw new UserLoginException("用户名或密码错误！");
        }

        //获取登录IP
        userDetails.setIp(RequestUtil.getRequestIp());

        // 先取角色的权限
        userDetails.setPermissions(sysMenuService.getRolePermissionByUserId(userDetails.getId()));
        userDetails.setIsAdmin(
                userDetails.getPermissions().contains(SecurityConstants.PERMISSION_ADMIN_NAME));
        // 再取菜单的权限
        Set<String> permissions = sysMenuService.getPermissionsByUserId(userDetails.getId(),
                userDetails.isAdmin());
        // 合并权限
        permissions.addAll(userDetails.getPermissions());
        if (this.removeFixedPermission) {
            permissions.remove(SecurityConstants.PERMISSION_ADMIN_NAME);
        }
        userDetails.setPermissions(permissions);

        return userDetails;
    }

    /**
     * 根据登录名查询
     *
     * @param username
     * @return
     */
    public abstract AbstractUserDetails findByUsername(String username);
}
