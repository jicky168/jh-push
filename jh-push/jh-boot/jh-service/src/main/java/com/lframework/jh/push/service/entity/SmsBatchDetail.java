package com.lframework.jh.push.service.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.core.entity.BaseEntity;
import com.lframework.jh.push.service.enums.SmsBatchDetailStatus;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("tbl_sms_batch_detail")
public class SmsBatchDetail extends BaseEntity implements BaseDto, Serializable {
    private static final long serialVersionUID = 1L;

    public static final String CACHE_NAME = "SmsBatchDetail";

    /**
     * ID
     */
    private String id;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 状态
     */
    private SmsBatchDetailStatus status;

    /**
     * 创建时间 新增时赋值
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间 新增和修改时赋值
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 成功时间
     */
    private LocalDateTime successTime;

    /**
     * 短信内容
     */
    private String smsContent;

    /**
     * 失败原因
     */
    private String errorMsg;

    /**
     * 业务ID
     */
    private String bizId;
}
