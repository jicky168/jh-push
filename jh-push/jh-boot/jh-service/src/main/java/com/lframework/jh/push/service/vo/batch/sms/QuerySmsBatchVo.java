package com.lframework.jh.push.service.vo.batch.sms;

import com.lframework.jh.push.core.components.validation.IsEnum;
import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.PageVo;
import com.lframework.jh.push.service.enums.SmsBatchDetailStatus;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QuerySmsBatchVo extends PageVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 接收手机号
     */
    @ApiModelProperty("接收手机号")
    private String phoneNumber;

    /**
     * 创建起始时间
     */
    @ApiModelProperty("创建起始时间")
    private LocalDateTime startTime;

    /**
     * 创建截止时间
     */
    @ApiModelProperty("创建截止时间")
    private LocalDateTime endTime;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    @IsEnum(message = "状态格式错误！", enumClass = SmsBatchDetailStatus.class)
    private Integer status;
}
