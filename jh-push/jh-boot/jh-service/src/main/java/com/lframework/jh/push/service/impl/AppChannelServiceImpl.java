package com.lframework.jh.push.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lframework.jh.push.core.impl.BaseMpServiceImpl;
import com.lframework.jh.push.service.entity.AppChannel;
import com.lframework.jh.push.service.mappers.AppChannelMapper;
import com.lframework.jh.push.service.service.AppChannelService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class AppChannelServiceImpl extends BaseMpServiceImpl<AppChannelMapper, AppChannel> implements AppChannelService {

    @Cacheable(value = AppChannel.CACHE_NAME, key = "#appId + '_' + #channelId", unless = "#result == null")
    @Override
    public AppChannel findByAppIdAndChannelId(String appId, String channelId) {
        Wrapper<AppChannel> queryWrapper = Wrappers.lambdaQuery(AppChannel.class)
                .eq(AppChannel::getAppId, appId)
                .eq(AppChannel::getChannelId, channelId);
        return this.getOne(queryWrapper);
    }

    @CacheEvict(value = AppChannel.CACHE_NAME, key = "#key")
    @Override
    public void cleanCacheByKey(Serializable key) {
        super.cleanCacheByKey(key);
    }
}
