package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.components.security.DefaultUserDetails;

/**
 * 用于登录的用户信息查询Mapper
 *
 * @author zmj
 */
public interface UserDetailsMapper {

    /**
     * 根据登录名查询
     *
     * @param username
     * @return
     */
    DefaultUserDetails findByUsername(String username);
}
