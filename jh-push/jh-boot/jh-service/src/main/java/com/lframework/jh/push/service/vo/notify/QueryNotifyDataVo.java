package com.lframework.jh.push.service.vo.notify;

import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QueryNotifyDataVo extends PageVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 通知URL
     */
    @ApiModelProperty("通知URL")
    private String notifyUrl;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    /**
     * 创建起始时间
     */
    @ApiModelProperty("创建起始时间")
    private LocalDateTime startTime;

    /**
     * 创建截止时间
     */
    @ApiModelProperty("创建截止时间")
    private LocalDateTime endTime;
}
