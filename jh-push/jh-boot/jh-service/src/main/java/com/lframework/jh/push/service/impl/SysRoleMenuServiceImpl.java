package com.lframework.jh.push.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lframework.jh.push.core.components.security.SecurityConstants;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.impl.BaseMpServiceImpl;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.IdUtil;
import com.lframework.jh.push.core.utils.ObjectUtil;
import com.lframework.jh.push.service.annotations.OpLog;
import com.lframework.jh.push.service.entity.SysRole;
import com.lframework.jh.push.service.entity.SysRoleMenu;
import com.lframework.jh.push.service.enums.OpLogType;
import com.lframework.jh.push.service.mappers.SysRoleMenuMapper;
import com.lframework.jh.push.service.service.SysRoleMenuService;
import com.lframework.jh.push.service.service.SysRoleService;
import com.lframework.jh.push.service.vo.role.SysRoleMenuSettingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SysRoleMenuServiceImpl extends
        BaseMpServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    @Autowired
    private SysRoleService sysRoleService;

    @OpLog(type = OpLogType.SYS, name = "角色授权菜单，角色ID：{}，菜单ID：{}", params = {"#vo.roleIds",
            "#vo.menuIds"}, loopFormat = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void setting(SysRoleMenuSettingVo vo) {

        for (String roleId : vo.getRoleIds()) {
            SysRole role = sysRoleService.findById(roleId);
            if (ObjectUtil.isNull(role)) {
                throw new DefaultClientException("角色不存在！");
            }

            if (SecurityConstants.PERMISSION_ADMIN_NAME.equals(role.getPermission())) {
                throw new DefaultClientException(
                        "角色【" + role.getName() + "】的权限为【" + SecurityConstants.PERMISSION_ADMIN_NAME
                                + "】，不允许授权！");
            }

            this.doSetting(roleId, vo.getMenuIds());
        }
    }

    protected void doSetting(String roleId, List<String> menuIds) {

        Wrapper<SysRoleMenu> deleteWrapper = Wrappers.lambdaQuery(SysRoleMenu.class)
                .eq(SysRoleMenu::getRoleId, roleId);
        getBaseMapper().delete(deleteWrapper);

        List<SysRoleMenu> records = new ArrayList<>();
        if (!CollectionUtil.isEmpty(menuIds)) {
            Set<String> menuIdSet = new HashSet<>(menuIds);

            for (String menuId : menuIdSet) {
                SysRoleMenu record = new SysRoleMenu();
                record.setId(IdUtil.getId());
                record.setRoleId(roleId);
                record.setMenuId(menuId);

                records.add(record);
            }
        }

        if (CollectionUtil.isNotEmpty(records)) {
            this.saveBatch(records);
        }
    }
}
