package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.dto.MenuDto;
import com.lframework.jh.push.service.entity.SysMenu;
import com.lframework.jh.push.service.vo.menu.SysMenuSelectorVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 系统菜单 Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-05-10
 */
public interface SysMenuMapper extends BaseMpMapper<SysMenu> {

    /**
     * 系统菜单列表
     *
     * @return
     */
    List<SysMenu> query();

    /**
     * 根据角色ID查询已授权的菜单
     *
     * @param roleId
     * @return
     */
    List<SysMenu> getByRoleId(@Param("roleId") String roleId);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    SysMenu findById(String id);

    /**
     * 系统菜单选择器数据
     *
     * @return
     */
    List<SysMenu> selector(@Param("vo") SysMenuSelectorVo vo);

    /**
     * 根据ID查询子节点
     *
     * @param id
     * @return
     */
    List<SysMenu> getChildrenById(String id);

    /**
     * 根据用户ID查询菜单
     *
     * @param userId
     * @param isAdmin 是否为管理员
     * @return
     */
    List<MenuDto> getMenuByUserId(@Param("userId") String userId, @Param("isAdmin") boolean isAdmin);

    /**
     * 根据用户ID查询收藏的菜单
     *
     * @param userId
     * @return
     */
    List<String> getCollectMenuIds(@Param("userId") String userId);

    /**
     * 根据用户ID查询权限
     *
     * @param userId
     * @return
     */
    Set<String> getPermissionsByUserId(@Param("userId") String userId,
                                       @Param("isAdmin") boolean isAdmin);

    /**
     * 根据用户ID查询角色权限
     *
     * @param userId
     * @return
     */
    Set<String> getRolePermissionByUserId(@Param("userId") String userId);

    /**
     * 收藏菜单
     *
     * @param id
     * @param userId
     * @param menuId
     */
    void collectMenu(@Param("id") String id, @Param("userId") String userId,
                     @Param("menuId") String menuId);

    /**
     * 取消收藏菜单
     *
     * @param userId
     * @param menuId
     */
    void cancelCollectMenu(@Param("userId") String userId, @Param("menuId") String menuId);
}
