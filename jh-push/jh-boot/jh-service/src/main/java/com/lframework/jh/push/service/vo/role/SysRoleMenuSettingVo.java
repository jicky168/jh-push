package com.lframework.jh.push.service.vo.role;

import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class SysRoleMenuSettingVo implements BaseVo {

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID", required = true)
    @NotEmpty(message = "角色ID不能为空！")
    private List<String> roleIds;

    /**
     * 菜单ID
     */
    @ApiModelProperty("菜单ID")
    private List<String> menuIds;
}
