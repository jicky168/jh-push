package com.lframework.jh.push.service.service;


import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.SysRoleMenu;
import com.lframework.jh.push.service.vo.role.SysRoleMenuSettingVo;

public interface SysRoleMenuService extends BaseMpService<SysRoleMenu> {

    /**
     * 授权角色菜单
     *
     * @param vo
     */
    void setting(SysRoleMenuSettingVo vo);
}
