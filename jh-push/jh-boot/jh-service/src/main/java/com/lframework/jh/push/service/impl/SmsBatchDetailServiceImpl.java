package com.lframework.jh.push.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lframework.jh.push.core.impl.BaseMpServiceImpl;
import com.lframework.jh.push.service.entity.SmsBatchDetail;
import com.lframework.jh.push.service.enums.SmsBatchDetailStatus;
import com.lframework.jh.push.service.mappers.SmsBatchDetailMapper;
import com.lframework.jh.push.service.service.SmsBatchDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SmsBatchDetailServiceImpl extends
    BaseMpServiceImpl<SmsBatchDetailMapper, SmsBatchDetail> implements SmsBatchDetailService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateFailByBatchNo(String batchNo, String errorMsg) {
        Wrapper<SmsBatchDetail> updateWrapper = Wrappers.lambdaUpdate(SmsBatchDetail.class)
            .eq(SmsBatchDetail::getBatchNo, batchNo)
            .set(SmsBatchDetail::getStatus, SmsBatchDetailStatus.FAIL)
            .set(SmsBatchDetail::getErrorMsg, errorMsg);
        this.update(updateWrapper);
    }
}
