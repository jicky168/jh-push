package com.lframework.jh.push.service.vo.order;

import com.lframework.jh.push.service.enums.PushOrderStatus;
import com.lframework.jh.push.service.enums.PushOrderType;
import com.lframework.jh.push.core.components.validation.IsEnum;
import com.lframework.jh.push.core.vo.BaseVo;
import com.lframework.jh.push.core.vo.PageVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class QueryPushOrderVo extends PageVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 应用ID
     */
    @ApiModelProperty("应用ID")
    private String appId;

    /**
     * 渠道
     */
    @ApiModelProperty("渠道")
    private String channel;

    /**
     * 外部单号
     */
    @ApiModelProperty("外部单号")
    private String outerNo;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    /**
     * 创建起始时间
     */
    @ApiModelProperty("创建起始时间")
    private LocalDateTime startTime;

    /**
     * 创建截止时间
     */
    @ApiModelProperty("创建截止时间")
    private LocalDateTime endTime;

    /**
     * 单据类型
     */
    @ApiModelProperty("单据类型")
    @IsEnum(message = "单据类型格式错误！", enumClass = PushOrderType.class)
    private Integer orderType;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    @IsEnum(message = "状态格式错误！", enumClass = PushOrderStatus.class)
    private Integer status;
}
