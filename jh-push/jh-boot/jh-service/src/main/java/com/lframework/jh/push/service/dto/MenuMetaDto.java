package com.lframework.jh.push.service.dto;

import com.lframework.jh.push.core.dto.BaseDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 菜单Meta Dto
 *
 * @author zmj
 */
@Data
public class MenuMetaDto implements BaseDto, Serializable {

    /**
     * 标题
     */
    @ApiModelProperty("标题")
    private String title;

    /**
     * 图标
     */
    @ApiModelProperty("图标")
    private String icon;

    /**
     * 是否不缓存
     */
    @ApiModelProperty("是否不缓存")
    private Boolean noCache;
}
