package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.NotifyData;
import org.apache.ibatis.annotations.Param;

public interface NotifyDataMapper extends BaseMpMapper<NotifyData> {

    /**
     * 根据ID增加通知次数
     * @param id
     * @param resp
     */
    void addNotifyCount(@Param("id") String id, @Param("resp") String resp);
}
