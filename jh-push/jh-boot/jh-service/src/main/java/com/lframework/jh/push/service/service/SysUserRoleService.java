package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.SysUserRole;
import com.lframework.jh.push.service.vo.user.SysUserRoleSettingVo;

import java.util.List;

public interface SysUserRoleService extends BaseMpService<SysUserRole> {

    /**
     * 用户授权
     *
     * @param vo
     */
    void setting(SysUserRoleSettingVo vo);

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    List<SysUserRole> getByUserId(String userId);
}
