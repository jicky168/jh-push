package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.AppChannel;

public interface AppChannelMapper extends BaseMpMapper<AppChannel> {
}
