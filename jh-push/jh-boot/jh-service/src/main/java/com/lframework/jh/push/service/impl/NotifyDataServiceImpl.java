package com.lframework.jh.push.service.impl;

import com.lframework.jh.push.core.impl.BaseMpServiceImpl;
import com.lframework.jh.push.service.entity.NotifyData;
import com.lframework.jh.push.service.mappers.NotifyDataMapper;
import com.lframework.jh.push.service.service.NotifyDataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NotifyDataServiceImpl extends BaseMpServiceImpl<NotifyDataMapper, NotifyData> implements NotifyDataService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addNotifyCount(String id, String resp) {
        getBaseMapper().addNotifyCount(id, resp);
    }
}
