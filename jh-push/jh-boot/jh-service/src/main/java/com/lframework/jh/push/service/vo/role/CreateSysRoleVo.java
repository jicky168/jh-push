package com.lframework.jh.push.service.vo.role;

import com.lframework.jh.push.core.components.validation.IsCode;
import com.lframework.jh.push.core.vo.BaseVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class CreateSysRoleVo implements BaseVo, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号", required = true)
    @IsCode
    @NotBlank(message = "请输入编号！")
    private String code;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称", required = true)
    @NotBlank(message = "请输入名称！")
    private String name;

    /**
     * 权限
     */
    @ApiModelProperty("权限")
    private String permission;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;
}
