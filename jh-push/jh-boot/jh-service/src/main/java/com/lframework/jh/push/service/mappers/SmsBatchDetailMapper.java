package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.SmsBatchDetail;

public interface SmsBatchDetailMapper extends BaseMpMapper<SmsBatchDetail> {
}
