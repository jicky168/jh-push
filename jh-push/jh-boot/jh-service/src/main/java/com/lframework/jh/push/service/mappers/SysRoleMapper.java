package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.SysRole;
import com.lframework.jh.push.service.vo.role.QuerySysRoleVo;
import com.lframework.jh.push.service.vo.role.SysRoleSelectorVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-07-02
 */
public interface SysRoleMapper extends BaseMpMapper<SysRole> {

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<SysRole> query(@Param("vo") QuerySysRoleVo vo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    SysRole findById(String id);

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    List<SysRole> getByUserId(String userId);

    /**
     * 选择器
     *
     * @param vo
     * @return
     */
    List<SysRole> selector(@Param("vo") SysRoleSelectorVo vo);
}
