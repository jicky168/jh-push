package com.lframework.jh.push.service.mappers;


import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.Mch;
import com.lframework.jh.push.service.vo.mch.MchSelectorVo;
import com.lframework.jh.push.service.vo.mch.QueryMchVo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-07-04
 */
public interface MchMapper extends BaseMpMapper<Mch> {

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<Mch> query(@Param("vo") QueryMchVo vo);

    /**
     * 选择器
     *
     * @param vo
     * @return
     */
    List<Mch> selector(@Param("vo") MchSelectorVo vo);
}
