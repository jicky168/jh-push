package com.lframework.jh.push.service.mappers;

import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.OpLogs;
import com.lframework.jh.push.service.vo.log.QueryOpLogsVo;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 操作日志 Mapper
 * </p>
 *
 * @author zmj
 */
public interface OpLogsMapper extends BaseMpMapper<OpLogs> {

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<OpLogs> query(@Param("vo") QueryOpLogsVo vo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    OpLogs findById(String id);

    /**
     * 根据截止时间删除日志
     *
     * @param endTime
     */
    void clearLogs(@Param("endTime") LocalDateTime endTime);
}
