package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.Channel;
import com.lframework.jh.push.service.vo.channel.ChannelSelectorVo;
import com.lframework.jh.push.service.vo.channel.CreateChannelVo;
import com.lframework.jh.push.service.vo.channel.QueryChannelVo;
import com.lframework.jh.push.service.vo.channel.UpdateChannelVo;

import java.util.List;

public interface ChannelService extends BaseMpService<Channel> {

    /**
     * 查询列表
     *
     * @return
     */
    PageResult<Channel> query(Integer pageIndex, Integer pageSize, QueryChannelVo vo);

    /**
     * 查询列表
     *
     * @param vo
     * @return
     */
    List<Channel> query(QueryChannelVo vo);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Channel findById(String id);

    /**
     * 根据编号查询
     * @param code
     * @return
     */
    Channel findByCode(String code);

    /**
     * 选择器
     *
     * @return
     */
    PageResult<Channel> selector(Integer pageIndex, Integer pageSize, ChannelSelectorVo vo);

    /**
     * 创建
     *
     * @param vo
     * @return
     */
    String create(CreateChannelVo vo);

    /**
     * 修改
     *
     * @param vo
     */
    void update(UpdateChannelVo vo);
}
