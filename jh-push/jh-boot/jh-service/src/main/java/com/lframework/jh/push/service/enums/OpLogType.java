package com.lframework.jh.push.service.enums;

/**
 * 默认操作日志类别
 *
 * @author zmj
 */
public interface OpLogType {

    /**
     * 用户认证
     */
    int AUTH = 1;

    /**
     * 应用
     */
    int APP = 2;

    /**
     * 渠道
     */
    int CHANNEL = 3;

    /**
     * 商户
     */
    int MCH = 4;

    /**
     * 系统管理
     */
    int SYS = 99;
}
