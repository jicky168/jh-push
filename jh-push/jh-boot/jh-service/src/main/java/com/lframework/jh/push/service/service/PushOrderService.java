package com.lframework.jh.push.service.service;

import com.lframework.jh.push.core.service.BaseMpService;
import com.lframework.jh.push.service.entity.PushOrder;

public interface PushOrderService extends BaseMpService<PushOrder> {
}
