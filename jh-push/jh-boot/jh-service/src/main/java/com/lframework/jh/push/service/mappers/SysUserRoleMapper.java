package com.lframework.jh.push.service.mappers;


import com.lframework.jh.push.core.mapper.BaseMpMapper;
import com.lframework.jh.push.service.entity.SysUserRole;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zmj
 * @since 2021-07-04
 */
public interface SysUserRoleMapper extends BaseMpMapper<SysUserRole> {

    /**
     * 根据用户ID查询
     *
     * @param userId
     * @return
     */
    List<SysUserRole> getByUserId(String userId);
}
