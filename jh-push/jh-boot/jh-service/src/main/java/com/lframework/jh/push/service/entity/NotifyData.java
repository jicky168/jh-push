package com.lframework.jh.push.service.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lframework.jh.push.core.dto.BaseDto;
import com.lframework.jh.push.core.entity.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("tbl_notify_data")
public class NotifyData extends BaseEntity implements BaseDto, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 商户ID
     */
    private Integer mchId;

    /**
     * 通知URL
     */
    private String notifyUrl;

    /**
     * 批次号
     */
    private String batchNo;

    /**
     * 通知内容
     */
    private String content;

    /**
     * 创建时间 新增时赋值
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 通知次数
     */
    private Integer notifyCount;

    /**
     * 第三方响应值
     */
    private String resp;
}
