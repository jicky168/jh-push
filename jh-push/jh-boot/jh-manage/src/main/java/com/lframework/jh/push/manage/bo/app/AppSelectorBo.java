package com.lframework.jh.push.manage.bo.app;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.service.entity.App;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppSelectorBo extends BaseBo<App> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    public AppSelectorBo() {

    }

    public AppSelectorBo(App dto) {

        super(dto);
    }
}
