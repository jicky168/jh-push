package com.lframework.jh.push.manage.bo.oplog;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.constants.StringPool;
import com.lframework.jh.push.service.entity.OpLogs;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QueryOpLogBo extends BaseBo<OpLogs> {

  /**
   * ID
   */
  @ApiModelProperty("ID")
  private String id;

  /**
   * 日志名称
   */
  @ApiModelProperty("日志名称")
  private String name;

  /**
   * 类别
   */
  @ApiModelProperty("类别")
  private Integer logType;

  /**
   * IP地址
   */
  @ApiModelProperty("IP地址")
  private String ip;

  /**
   * 创建人
   */
  @ApiModelProperty("创建人")
  private String createBy;

  /**
   * 创建时间
   */
  @ApiModelProperty("创建时间")
  @JsonFormat(pattern = StringPool.DATE_TIME_PATTERN)
  private LocalDateTime createTime;

  public QueryOpLogBo() {

  }

  public QueryOpLogBo(OpLogs dto) {

    super(dto);
  }
}
