package com.lframework.jh.push.manage.bo.notify;

import com.lframework.jh.push.service.entity.NotifyData;
import com.lframework.jh.push.core.bo.BaseBo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QueryNotifyDataBo extends BaseBo<NotifyData> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    /**
     * 通知URL
     */
    @ApiModelProperty("通知URL")
    private String notifyUrl;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 通知次数
     */
    @ApiModelProperty("通知次数")
    private Integer notifyCount;

    public QueryNotifyDataBo() {
    }

    public QueryNotifyDataBo(NotifyData dto) {
        super(dto);
    }
}
