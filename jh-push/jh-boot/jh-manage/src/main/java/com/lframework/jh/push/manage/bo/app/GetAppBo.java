package com.lframework.jh.push.manage.bo.app;

import com.lframework.jh.push.core.annotations.convert.EncryptConvert;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.service.entity.App;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetAppBo extends BaseBo<App> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;

    public GetAppBo() {

    }

    public GetAppBo(App dto) {

        super(dto);
    }
}
