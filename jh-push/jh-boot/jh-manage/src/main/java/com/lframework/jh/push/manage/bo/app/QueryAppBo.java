package com.lframework.jh.push.manage.bo.app;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.constants.StringPool;
import com.lframework.jh.push.service.entity.App;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QueryAppBo extends BaseBo<App> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = StringPool.DATE_TIME_PATTERN)
    private LocalDateTime createTime;

    /**
     * 修改人
     */
    @ApiModelProperty("修改人")
    private String updateBy;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = StringPool.DATE_TIME_PATTERN)
    private LocalDateTime updateTime;

    public QueryAppBo() {

    }

    public QueryAppBo(App dto) {

        super(dto);
    }

    @Override
    protected void afterInit(App dto) {
    }
}
