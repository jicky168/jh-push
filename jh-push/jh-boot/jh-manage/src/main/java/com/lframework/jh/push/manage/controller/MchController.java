package com.lframework.jh.push.manage.controller;

import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.controller.DefaultBaseController;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.PageResultUtil;
import com.lframework.jh.push.manage.bo.mch.GetMchBo;
import com.lframework.jh.push.manage.bo.mch.QueryMchBo;
import com.lframework.jh.push.service.entity.Mch;
import com.lframework.jh.push.service.service.MchService;
import com.lframework.jh.push.service.vo.mch.CreateMchVo;
import com.lframework.jh.push.service.vo.mch.QueryMchVo;
import com.lframework.jh.push.service.vo.mch.UpdateMchVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商户管理
 *
 * @author zmj
 */
@Api(tags = "商户管理")
@Validated
@RestController
@RequestMapping("/mch")
public class MchController extends DefaultBaseController {

    @Autowired
    private MchService mchService;

    /**
     * 查询列表
     */
    @ApiOperation("查询列表")
    @HasPermission({"mch:query", "mch:add", "mch:modify"})
    @GetMapping("/query")
    public InvokeResult<PageResult<QueryMchBo>> query(@Valid QueryMchVo vo) {

        PageResult<Mch> pageResult = mchService.query(getPageIndex(vo),
            getPageSize(vo), vo);

        List<Mch> datas = pageResult.getDatas();
        List<QueryMchBo> results = null;

        if (!CollectionUtil.isEmpty(datas)) {
            results = datas.stream().map(QueryMchBo::new).collect(Collectors.toList());
        }

        return InvokeResultBuilder.success(PageResultUtil.rebuild(pageResult, results));
    }

    /**
     * 查询详情
     */
    @ApiOperation("查询详情")
    @ApiImplicitParam(value = "ID", name = "id", paramType = "query", required = true)
    @HasPermission({"mch:query", "mch:add", "mch:modify"})
    @GetMapping
    public InvokeResult<GetMchBo> get(@NotNull(message = "ID不能为空！") Integer id) {

        Mch data = mchService.findById(id);
        if (data == null) {
            throw new DefaultClientException("商户不存在！");
        }

        GetMchBo result = new GetMchBo(data);

        return InvokeResultBuilder.success(result);
    }

    /**
     * 新增
     */
    @ApiOperation("新增")
    @HasPermission({"mch:add"})
    @PostMapping
    public InvokeResult<Void> create(@Valid CreateMchVo vo) {

        mchService.create(vo);

        return InvokeResultBuilder.success();
    }

    /**
     * 修改
     */
    @ApiOperation("修改")
    @HasPermission({"mch:modify"})
    @PutMapping
    public InvokeResult<Void> update(@Valid UpdateMchVo vo) {

        mchService.update(vo);

        mchService.cleanCacheByKey(vo.getId());

        return InvokeResultBuilder.success();
    }
}
