package com.lframework.jh.push.manage;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@ServletComponentScan(basePackages = {"com.lframework.jh.push"})
@SpringBootApplication(scanBasePackages = {"com.lframework.jh.push"})
@MapperScan({"com.lframework.jh.push.**.mappers"})
public class ManageApplication {

    public static void main(String[] args) {

        SpringApplication.run(ManageApplication.class, args);
    }

    /**
     * Swagger 自定义配置信息 请自行修改
     */
    @Configuration
    public static class Knife4jApiConfiguration {

        @Bean(value = "defaultApi")
        public Docket defaultApiDocket(OpenApiExtensionResolver openApiExtensionResolver) {

            Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).groupName("聚合推送")
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.lframework.jh.push.manage"))
                    .build().extensions(openApiExtensionResolver.buildSettingExtensions());
            return docket;
        }

        // 可以修改内容 但是不要删除这个Bean
        @Bean
        public ApiInfo apiInfo() {

            return new ApiInfoBuilder().title("聚合推送接口文档").description("# 聚合推送接口文档")
                    .contact("lframework@163.com")
                    .build();
        }
    }
}
