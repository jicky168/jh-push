package com.lframework.jh.push.manage.bo.chart;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.dto.VoidDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TodayCountLineChartBo extends BaseBo<VoidDto> {

    /**
     * 小时
     */
    @ApiModelProperty("小时")
    private String createHour;

    /**
     * 数量
     */
    @ApiModelProperty("数量")
    private Long count;
}
