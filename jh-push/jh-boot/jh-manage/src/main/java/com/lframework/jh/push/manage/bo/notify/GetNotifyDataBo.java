package com.lframework.jh.push.manage.bo.notify;

import com.lframework.jh.push.service.entity.NotifyData;
import com.lframework.jh.push.core.bo.BaseBo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GetNotifyDataBo extends BaseBo<NotifyData> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    /**
     * 通知URL
     */
    @ApiModelProperty("通知URL")
    private String notifyUrl;

    /**
     * 通知内容
     */
    @ApiModelProperty("通知内容")
    private String content;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 通知次数
     */
    @ApiModelProperty("通知次数")
    private Integer notifyCount;

    /**
     * 响应值
     */
    @ApiModelProperty("响应值")
    private String resp;

    public GetNotifyDataBo() {
    }

    public GetNotifyDataBo(NotifyData dto) {
        super(dto);
    }
}
