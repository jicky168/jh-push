package com.lframework.jh.push.manage.controller;

import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.controller.DefaultBaseController;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.PageResultUtil;
import com.lframework.jh.push.manage.bo.role.GetSysRoleBo;
import com.lframework.jh.push.manage.bo.role.QuerySysRoleBo;
import com.lframework.jh.push.service.entity.SysRole;
import com.lframework.jh.push.service.service.SysRoleService;
import com.lframework.jh.push.service.vo.role.CreateSysRoleVo;
import com.lframework.jh.push.service.vo.role.QuerySysRoleVo;
import com.lframework.jh.push.service.vo.role.UpdateSysRoleVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色管理
 *
 * @author zmj
 */
@Api(tags = "角色管理")
@Validated
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends DefaultBaseController {

    @Autowired
    private SysRoleService sysRoleService;

    /**
     * 角色列表
     */
    @ApiOperation("角色列表")
    @HasPermission({"system:role:query", "system:role:add", "system:role:modify"})
    @GetMapping("/query")
    public InvokeResult<PageResult<QuerySysRoleBo>> query(@Valid QuerySysRoleVo vo) {

        PageResult<SysRole> pageResult = sysRoleService.query(getPageIndex(vo),
                getPageSize(vo), vo);

        List<SysRole> datas = pageResult.getDatas();
        List<QuerySysRoleBo> results = null;

        if (!CollectionUtil.isEmpty(datas)) {
            results = datas.stream().map(QuerySysRoleBo::new).collect(Collectors.toList());
        }

        return InvokeResultBuilder.success(PageResultUtil.rebuild(pageResult, results));
    }

    /**
     * 查询角色
     */
    @ApiOperation("查询角色")
    @ApiImplicitParam(value = "ID", name = "id", paramType = "query", required = true)
    @HasPermission({"system:role:query", "system:role:add", "system:role:modify"})
    @GetMapping
    public InvokeResult<GetSysRoleBo> get(@NotBlank(message = "ID不能为空！") String id) {

        SysRole data = sysRoleService.findById(id);
        if (data == null) {
            throw new DefaultClientException("角色不存在！");
        }

        GetSysRoleBo result = new GetSysRoleBo(data);

        return InvokeResultBuilder.success(result);
    }

    /**
     * 批量停用角色
     */
    @ApiOperation("批量停用角色")
    @HasPermission({"system:role:modify"})
    @PatchMapping("/unable/batch")
    public InvokeResult<Void> batchUnable(
            @ApiParam(value = "角色ID", required = true) @NotEmpty(message = "请选择需要停用的角色！") @RequestBody List<String> ids) {

        sysRoleService.batchUnable(ids);

        for (String id : ids) {
            sysRoleService.cleanCacheByKey(id);
        }

        return InvokeResultBuilder.success();
    }

    /**
     * 批量启用角色
     */
    @ApiOperation("批量启用角色")
    @HasPermission({"system:role:modify"})
    @PatchMapping("/enable/batch")
    public InvokeResult<Void> batchEnable(
            @ApiParam(value = "角色ID", required = true) @NotEmpty(message = "请选择需要启用的角色！") @RequestBody List<String> ids) {

        sysRoleService.batchEnable(ids);

        for (String id : ids) {
            sysRoleService.cleanCacheByKey(id);
        }

        return InvokeResultBuilder.success();
    }

    /**
     * 新增角色
     */
    @ApiOperation("新增角色")
    @HasPermission({"system:role:add"})
    @PostMapping
    public InvokeResult<Void> create(@Valid CreateSysRoleVo vo) {

        sysRoleService.create(vo);

        return InvokeResultBuilder.success();
    }

    /**
     * 修改角色
     */
    @ApiOperation("修改角色")
    @HasPermission({"system:role:modify"})
    @PutMapping
    public InvokeResult<Void> update(@Valid UpdateSysRoleVo vo) {

        sysRoleService.update(vo);

        sysRoleService.cleanCacheByKey(vo.getId());

        return InvokeResultBuilder.success();
    }
}
