package com.lframework.jh.push.manage.bo.user;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.constants.StringPool;
import com.lframework.jh.push.core.dto.VoidDto;
import com.lframework.jh.push.core.utils.ApplicationUtil;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.StringUtil;
import com.lframework.jh.push.service.entity.SysRole;
import com.lframework.jh.push.service.entity.SysUser;
import com.lframework.jh.push.service.entity.SysUserRole;
import com.lframework.jh.push.service.service.SysRoleService;
import com.lframework.jh.push.service.service.SysUserRoleService;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
public class GetSysUserBo extends BaseBo<SysUser> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 编号
     */
    @ApiModelProperty("编号")
    private String code;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String email;

    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String telephone;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private Integer gender;

    /**
     * 角色
     */
    @ApiModelProperty("角色")
    private List<String> roles;

    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String roleName;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    /**
     * 是否锁定
     */
    @ApiModelProperty("是否锁定")
    private Boolean lockStatus;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;

    public GetSysUserBo() {

    }

    public GetSysUserBo(SysUser dto) {

        super(dto);
    }

    @Override
    protected void afterInit(SysUser dto) {

        SysUserRoleService sysUserRoleService = ApplicationUtil.getBean(SysUserRoleService.class);
        List<SysUserRole> userRoles = sysUserRoleService.getByUserId(dto.getId());
        if (!CollectionUtil.isEmpty(userRoles)) {
            SysRoleService sysRoleService = ApplicationUtil.getBean(SysRoleService.class);
            this.roles = userRoles.stream().map(SysUserRole::getRoleId)
                    .collect(Collectors.toList());

            this.roleName = StringUtil.join(StringPool.STR_SPLIT_CN,
                    userRoles.stream().map(t -> sysRoleService.findById(t.getRoleId()))
                            .filter(Objects::nonNull).map(SysRole::getName)
                            .collect(Collectors.toList()));
        }
    }

    @Data
    public static class PositionBo extends BaseBo<VoidDto> {

        /**
         * 岗位ID
         */
        @ApiModelProperty("岗位ID")
        private String id;

        /**
         * 岗位名称
         */
        @ApiModelProperty("岗位名称")
        private String name;
    }

    @Data
    public static class DeptBo extends BaseBo<VoidDto> {

        /**
         * 部门ID
         */
        @ApiModelProperty("部门ID")
        private String id;

        /**
         * 部门名称
         */
        @ApiModelProperty("部门名称")
        private String name;
    }

    @Data
    public static class RoleBo extends BaseBo<VoidDto> {

        /**
         * 角色ID
         */
        @ApiModelProperty("角色ID")
        private String id;

        /**
         * 角色名称
         */
        @ApiModelProperty("角色名称")
        private String name;
    }
}
