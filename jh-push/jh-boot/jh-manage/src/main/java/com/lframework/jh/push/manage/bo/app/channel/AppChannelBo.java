package com.lframework.jh.push.manage.bo.app.channel;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.dto.VoidDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppChannelBo extends BaseBo<VoidDto> {

    /**
     * 渠道ID
     */
    @ApiModelProperty("渠道ID")
    private String channelId;

    /**
     * 渠道名称
     */
    @ApiModelProperty("渠道名称")
    private String channelName;

    /**
     * 是否配置
     */
    @ApiModelProperty("是否配置")
    private Boolean isConfig;

    /**
     * 是否开通
     */
    @ApiModelProperty("是否开通")
    private Boolean available;
}
