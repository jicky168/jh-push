package com.lframework.jh.push.manage.bo.channel;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.service.entity.Channel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ChannelSelectorBo extends BaseBo<Channel> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 编号
     */
    @ApiModelProperty("编号")
    private String code;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    public ChannelSelectorBo() {

    }

    public ChannelSelectorBo(Channel dto) {

        super(dto);
    }
}
