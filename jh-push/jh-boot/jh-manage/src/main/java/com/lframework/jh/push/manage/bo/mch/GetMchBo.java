package com.lframework.jh.push.manage.bo.mch;

import com.lframework.jh.push.core.annotations.convert.EncryptConvert;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.service.entity.Mch;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetMchBo extends BaseBo<Mch> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;

    @ApiModelProperty("通信秘钥")
    @EncryptConvert
    private String apiSecret;

    public GetMchBo() {

    }

    public GetMchBo(Mch dto) {

        super(dto);
    }
}
