package com.lframework.jh.push.manage.controller;

import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.controller.DefaultBaseController;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.PageResultUtil;
import com.lframework.jh.push.manage.bo.app.GetAppBo;
import com.lframework.jh.push.manage.bo.app.QueryAppBo;
import com.lframework.jh.push.service.entity.App;
import com.lframework.jh.push.service.service.AppService;
import com.lframework.jh.push.service.vo.app.CreateAppVo;
import com.lframework.jh.push.service.vo.app.QueryAppVo;
import com.lframework.jh.push.service.vo.app.UpdateAppVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 应用管理
 *
 * @author zmj
 */
@Api(tags = "应用管理")
@Validated
@RestController
@RequestMapping("/app")
public class AppController extends DefaultBaseController {

    @Autowired
    private AppService appService;

    /**
     * 查询列表
     */
    @ApiOperation("查询列表")
    @HasPermission({"app:query", "app:add", "app:modify"})
    @GetMapping("/query")
    public InvokeResult<PageResult<QueryAppBo>> query(@Valid QueryAppVo vo) {

        PageResult<App> pageResult = appService.query(getPageIndex(vo),
                getPageSize(vo), vo);

        List<App> datas = pageResult.getDatas();
        List<QueryAppBo> results = null;

        if (!CollectionUtil.isEmpty(datas)) {
            results = datas.stream().map(QueryAppBo::new).collect(Collectors.toList());
        }

        return InvokeResultBuilder.success(PageResultUtil.rebuild(pageResult, results));
    }

    /**
     * 查询详情
     */
    @ApiOperation("查询详情")
    @ApiImplicitParam(value = "ID", name = "id", paramType = "query", required = true)
    @HasPermission({"app:query", "app:add", "app:modify"})
    @GetMapping
    public InvokeResult<GetAppBo> get(@NotBlank(message = "ID不能为空！") String id) {

        App data = appService.findById(id);
        if (data == null) {
            throw new DefaultClientException("应用不存在！");
        }

        GetAppBo result = new GetAppBo(data);

        return InvokeResultBuilder.success(result);
    }

    /**
     * 新增
     */
    @ApiOperation("新增")
    @HasPermission({"app:add"})
    @PostMapping
    public InvokeResult<Void> create(@Valid CreateAppVo vo) {

        appService.create(vo);

        return InvokeResultBuilder.success();
    }

    /**
     * 修改
     */
    @ApiOperation("修改")
    @HasPermission({"app:modify"})
    @PutMapping
    public InvokeResult<Void> update(@Valid UpdateAppVo vo) {

        appService.update(vo);

        appService.cleanCacheByKey(vo.getId());

        return InvokeResultBuilder.success();
    }
}
