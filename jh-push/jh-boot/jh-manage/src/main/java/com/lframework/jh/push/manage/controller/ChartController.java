package com.lframework.jh.push.manage.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lframework.jh.push.manage.bo.chart.MonthCountLineChartBo;
import com.lframework.jh.push.manage.bo.chart.TodayCountLineChartBo;
import com.lframework.jh.push.service.entity.PushOrder;
import com.lframework.jh.push.service.entity.SmsBatchDetail;
import com.lframework.jh.push.service.service.PushOrderService;
import com.lframework.jh.push.service.service.SmsBatchDetailService;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.controller.DefaultBaseController;
import com.lframework.jh.push.core.utils.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 图表报表
 *
 * @author zmj
 */
@Api(tags = "图表报表")
@Validated
@RestController
@RequestMapping("/chart")
public class ChartController extends DefaultBaseController {

    @Autowired
    private PushOrderService pushOrderService;

    @Autowired
    private SmsBatchDetailService smsBatchDetailService;

    /**
     * 今日推送单数量
     */
    @ApiOperation("今日推送单数量")
    @GetMapping("/order/today")
    public InvokeResult<Integer> todayOrder() {
        LocalDate now = LocalDate.now();
        Wrapper<PushOrder> queryWrapper = Wrappers.lambdaQuery(PushOrder.class)
                .ge(PushOrder::getCreateTime, DateUtil.toLocalDateTime(now))
                .le(PushOrder::getCreateTime, DateUtil.toLocalDateTimeMax(now));
        return InvokeResultBuilder.success(pushOrderService.count(queryWrapper));
    }

    /**
     * 今日推送单数量折线图
     */
    @ApiOperation("今日推送单数量折线图")
    @GetMapping("/order/today/line")
    public InvokeResult<List<TodayCountLineChartBo>> todayOrderLine() {
        LocalDate now = LocalDate.now();
        Wrapper<PushOrder> queryWrapper = Wrappers.lambdaQuery(PushOrder.class)
                .ge(PushOrder::getCreateTime, DateUtil.toLocalDateTime(now))
                .le(PushOrder::getCreateTime, DateUtil.toLocalDateTimeMax(now));
        List<PushOrder> datas = pushOrderService.list(queryWrapper);
        List<TodayCountLineChartBo> results = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            TodayCountLineChartBo result = new TodayCountLineChartBo();
            result.setCreateHour((i < 10 ? "0" : "") + i);
            int finalI = i;
            result.setCount(datas.stream().map(PushOrder::getCreateTime).filter(t -> t.getHour() == finalI).count());
            results.add(result);
        }
        return InvokeResultBuilder.success(results);
    }

    /**
     * 本月推送单数量
     */
    @ApiOperation("本月推送单数量")
    @GetMapping("/order/month")
    public InvokeResult<Integer> monthOrder() {
        LocalDate now = LocalDate.now();
        LocalDateTime startTime = DateUtil.toLocalDateTime(now).withDayOfMonth(1);
        LocalDateTime endTime = DateUtil.toLocalDateTimeMax(now).plusMonths(1).withDayOfMonth(1).minusDays(1);
        Wrapper<PushOrder> queryWrapper = Wrappers.lambdaQuery(PushOrder.class)
                .ge(PushOrder::getCreateTime, startTime)
                .le(PushOrder::getCreateTime, endTime);
        return InvokeResultBuilder.success(pushOrderService.count(queryWrapper));
    }

    /**
     * 本月推送单数量折线图
     */
    @ApiOperation("本月推送单数量折线图")
    @GetMapping("/order/month/line")
    public InvokeResult<List<MonthCountLineChartBo>> monthOrderLine() {
        LocalDate now = LocalDate.now();
        LocalDateTime startTime = DateUtil.toLocalDateTime(now).withDayOfMonth(1);
        LocalDateTime endTime = DateUtil.toLocalDateTimeMax(now).plusMonths(1).withDayOfMonth(1).minusDays(1);
        Wrapper<PushOrder> queryWrapper = Wrappers.lambdaQuery(PushOrder.class)
                .ge(PushOrder::getCreateTime, startTime)
                .le(PushOrder::getCreateTime, endTime);
        List<PushOrder> datas = pushOrderService.list(queryWrapper);
        List<MonthCountLineChartBo> results = new ArrayList<>();
        for (int i = 0; i < endTime.getDayOfMonth(); i++) {
            MonthCountLineChartBo result = new MonthCountLineChartBo();
            result.setCreateDate(DateUtil.formatDateTime(startTime.plusDays(i), "dd日"));
            int finalI = i;
            result.setCount(datas.stream().map(PushOrder::getCreateTime).filter(t -> t.getDayOfMonth() == (finalI + 1)).count());
            results.add(result);
        }
        return InvokeResultBuilder.success(results);
    }

    /**
     * 今日短信数量
     */
    @ApiOperation("今日短信数量")
    @GetMapping("/sms/today")
    public InvokeResult<Integer> todaySms() {
        LocalDate now = LocalDate.now();
        Wrapper<SmsBatchDetail> queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
                .ge(SmsBatchDetail::getCreateTime, DateUtil.toLocalDateTime(now))
                .le(SmsBatchDetail::getCreateTime, DateUtil.toLocalDateTimeMax(now));
        return InvokeResultBuilder.success(smsBatchDetailService.count(queryWrapper));
    }

    /**
     * 今日短信数量折线图
     */
    @ApiOperation("今日短信数量折线图")
    @GetMapping("/sms/today/line")
    public InvokeResult<List<TodayCountLineChartBo>> todaySmsLine() {
        LocalDate now = LocalDate.now();
        Wrapper<SmsBatchDetail> queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
                .ge(SmsBatchDetail::getCreateTime, DateUtil.toLocalDateTime(now))
                .le(SmsBatchDetail::getCreateTime, DateUtil.toLocalDateTimeMax(now));
        List<SmsBatchDetail> datas = smsBatchDetailService.list(queryWrapper);
        List<TodayCountLineChartBo> results = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            TodayCountLineChartBo result = new TodayCountLineChartBo();
            result.setCreateHour((i < 10 ? "0" : "") + i);
            int finalI = i;
            result.setCount(datas.stream().map(SmsBatchDetail::getCreateTime).filter(t -> t.getHour() == finalI).count());
            results.add(result);
        }
        return InvokeResultBuilder.success(results);
    }

    /**
     * 本月短信数量
     */
    @ApiOperation("本月短信数量")
    @GetMapping("/sms/month")
    public InvokeResult<Integer> monthSms() {
        LocalDate now = LocalDate.now();
        LocalDateTime startTime = DateUtil.toLocalDateTime(now).withDayOfMonth(1);
        LocalDateTime endTime = DateUtil.toLocalDateTimeMax(now).plusMonths(1).withDayOfMonth(1).minusDays(1);
        Wrapper<SmsBatchDetail> queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
                .ge(SmsBatchDetail::getCreateTime, startTime)
                .le(SmsBatchDetail::getCreateTime, endTime);
        return InvokeResultBuilder.success(smsBatchDetailService.count(queryWrapper));
    }

    /**
     * 本月短信数量折线图
     */
    @ApiOperation("本月短信数量折线图")
    @GetMapping("/sms/month/line")
    public InvokeResult<List<MonthCountLineChartBo>> monthSmsLine() {
        LocalDate now = LocalDate.now();
        LocalDateTime startTime = DateUtil.toLocalDateTime(now).withDayOfMonth(1);
        LocalDateTime endTime = DateUtil.toLocalDateTimeMax(now).plusMonths(1).withDayOfMonth(1).minusDays(1);
        Wrapper<SmsBatchDetail> queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
                .ge(SmsBatchDetail::getCreateTime, startTime)
                .le(SmsBatchDetail::getCreateTime, endTime);
        List<SmsBatchDetail> datas = smsBatchDetailService.list(queryWrapper);
        List<MonthCountLineChartBo> results = new ArrayList<>();
        for (int i = 0; i < endTime.getDayOfMonth(); i++) {
            MonthCountLineChartBo result = new MonthCountLineChartBo();
            result.setCreateDate(DateUtil.formatDateTime(startTime.plusDays(i), "dd日"));
            int finalI = i;
            result.setCount(datas.stream().map(SmsBatchDetail::getCreateTime).filter(t -> t.getDayOfMonth() == (finalI + 1)).count());
            results.add(result);
        }
        return InvokeResultBuilder.success(results);
    }
}
