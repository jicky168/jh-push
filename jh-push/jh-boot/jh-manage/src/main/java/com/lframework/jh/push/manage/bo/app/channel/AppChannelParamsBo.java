package com.lframework.jh.push.manage.bo.app.channel;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.dto.VoidDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AppChannelParamsBo extends BaseBo<VoidDto> {

    /**
     * 参数模板
     */
    @ApiModelProperty("参数模板")
    private String templateParams;

    /**
     * 参数
     */
    @ApiModelProperty("参数")
    private String params;

    /**
     * 是否开通
     */
    @ApiModelProperty("是否开通")
    private Boolean available;
}
