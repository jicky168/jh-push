package com.lframework.jh.push.manage.bo.order;

import com.lframework.jh.push.core.annotations.convert.EnumConvert;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.service.entity.PushOrder;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QueryPushOrderBo extends BaseBo<PushOrder> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 应用ID
     */
    @ApiModelProperty("应用ID")
    private String appId;

    /**
     * 渠道
     */
    @ApiModelProperty("渠道")
    private String channel;

    /**
     * 外部单号
     */
    @ApiModelProperty("外部单号")
    private String outerNo;

    /**
     * 通知Url
     */
    @ApiModelProperty("通知Url")
    private String notifyUrl;

    /**
     * 推送单类型
     */
    @ApiModelProperty("推送单类型")
    @EnumConvert
    private Integer orderType;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    @EnumConvert
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 成功时间
     */
    @ApiModelProperty("成功时间")
    private LocalDateTime successTime;

    /**
     * 失败时间
     */
    @ApiModelProperty("失败时间")
    private LocalDateTime failureTime;

    /**
     * 失败原因
     */
    @ApiModelProperty("失败原因")
    private String errorMsg;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    public QueryPushOrderBo() {
    }

    public QueryPushOrderBo(PushOrder dto) {
        super(dto);
    }
}
