package com.lframework.jh.push.manage.bo.chart;

import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.dto.VoidDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MonthCountLineChartBo extends BaseBo<VoidDto> {

    /**
     * 天数
     */
    @ApiModelProperty("天数")
    private String createDate;

    /**
     * 数量
     */
    @ApiModelProperty("数量")
    private Long count;
}
