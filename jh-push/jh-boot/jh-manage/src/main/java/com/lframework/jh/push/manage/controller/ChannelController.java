package com.lframework.jh.push.manage.controller;

import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.controller.DefaultBaseController;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.PageResultUtil;
import com.lframework.jh.push.manage.bo.channel.GetChannelBo;
import com.lframework.jh.push.manage.bo.channel.QueryChannelBo;
import com.lframework.jh.push.service.entity.Channel;
import com.lframework.jh.push.service.service.ChannelService;
import com.lframework.jh.push.service.vo.channel.CreateChannelVo;
import com.lframework.jh.push.service.vo.channel.QueryChannelVo;
import com.lframework.jh.push.service.vo.channel.UpdateChannelVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 渠道管理
 *
 * @author zmj
 */
@Api(tags = "渠道管理")
@Validated
@RestController
@RequestMapping("/channel")
public class ChannelController extends DefaultBaseController {

    @Autowired
    private ChannelService channelService;

    /**
     * 查询列表
     */
    @ApiOperation("查询列表")
    @HasPermission({"channel:query", "channel:add", "channel:modify"})
    @GetMapping("/query")
    public InvokeResult<PageResult<QueryChannelBo>> query(@Valid QueryChannelVo vo) {

        PageResult<Channel> pageResult = channelService.query(getPageIndex(vo),
                getPageSize(vo), vo);

        List<Channel> datas = pageResult.getDatas();
        List<QueryChannelBo> results = null;

        if (!CollectionUtil.isEmpty(datas)) {
            results = datas.stream().map(QueryChannelBo::new).collect(Collectors.toList());
        }

        return InvokeResultBuilder.success(PageResultUtil.rebuild(pageResult, results));
    }

    /**
     * 查询详情
     */
    @ApiOperation("查询详情")
    @ApiImplicitParam(value = "ID", name = "id", paramType = "query", required = true)
    @HasPermission({"channel:query", "channel:add", "channel:modify"})
    @GetMapping
    public InvokeResult<GetChannelBo> get(@NotBlank(message = "ID不能为空！") String id) {

        Channel data = channelService.findById(id);
        if (data == null) {
            throw new DefaultClientException("渠道不存在！");
        }

        GetChannelBo result = new GetChannelBo(data);

        return InvokeResultBuilder.success(result);
    }

    /**
     * 新增
     */
    @ApiOperation("新增")
    @HasPermission({"channel:add"})
    @PostMapping
    public InvokeResult<Void> create(@Valid @RequestBody CreateChannelVo vo) {

        channelService.create(vo);

        return InvokeResultBuilder.success();
    }

    /**
     * 修改
     */
    @ApiOperation("修改")
    @HasPermission({"channel:modify"})
    @PutMapping
    public InvokeResult<Void> update(@Valid @RequestBody UpdateChannelVo vo) {

        channelService.update(vo);

        channelService.cleanCacheByKey(vo.getId());
        channelService.cleanCacheByKey(vo.getCode());

        return InvokeResultBuilder.success();
    }
}
