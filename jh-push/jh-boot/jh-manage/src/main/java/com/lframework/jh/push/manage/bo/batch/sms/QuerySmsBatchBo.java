package com.lframework.jh.push.manage.bo.batch.sms;

import com.lframework.jh.push.core.annotations.convert.EnumConvert;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.service.entity.SmsBatchDetail;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QuerySmsBatchBo extends BaseBo<SmsBatchDetail> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 商户ID
     */
    @ApiModelProperty("商户ID")
    private Integer mchId;

    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private String batchNo;

    /**
     * 接收手机号
     */
    @ApiModelProperty("接收手机号")
    private String phoneNumber;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    @EnumConvert
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /**
     * 成功时间
     */
    @ApiModelProperty("成功时间")
    private LocalDateTime successTime;

    /**
     * 短信内容
     */
    @ApiModelProperty("短信内容")
    private String smsContent;

    /**
     * 失败原因
     */
    @ApiModelProperty("失败原因")
    private String errorMsg;

    public QuerySmsBatchBo() {
    }

    public QuerySmsBatchBo(SmsBatchDetail dto) {
        super(dto);
    }
}
