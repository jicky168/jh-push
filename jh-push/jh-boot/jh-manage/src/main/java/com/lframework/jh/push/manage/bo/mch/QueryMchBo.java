package com.lframework.jh.push.manage.bo.mch;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lframework.jh.push.core.bo.BaseBo;
import com.lframework.jh.push.core.constants.StringPool;
import com.lframework.jh.push.service.entity.Mch;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class QueryMchBo extends BaseBo<Mch> {

    /**
     * ID
     */
    @ApiModelProperty("ID")
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 状态
     */
    @ApiModelProperty("状态")
    private Boolean available;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String description;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = StringPool.DATE_TIME_PATTERN)
    private LocalDateTime createTime;

    /**
     * 修改人
     */
    @ApiModelProperty("修改人")
    private String updateBy;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = StringPool.DATE_TIME_PATTERN)
    private LocalDateTime updateTime;

    public QueryMchBo() {

    }

    public QueryMchBo(Mch dto) {

        super(dto);
    }

    @Override
    protected void afterInit(Mch dto) {
    }
}
