package com.lframework.jh.push.manage.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.PageResultUtil;
import com.lframework.jh.push.core.utils.StringUtil;
import com.lframework.jh.push.manage.bo.batch.sms.GetSmsBatchBo;
import com.lframework.jh.push.manage.bo.batch.sms.QuerySmsBatchBo;
import com.lframework.jh.push.service.entity.SmsBatchDetail;
import com.lframework.jh.push.service.service.SmsBatchDetailService;
import com.lframework.jh.push.service.vo.batch.sms.QuerySmsBatchVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 短信批次查询
 *
 * @author zmj
 */
@Api(tags = "短信批次查询")
@Validated
@RestController
@RequestMapping("/batch/sms")
public class SmsBatchController {

    @Autowired
    private SmsBatchDetailService smsBatchDetailService;

    /**
     * 查询列表
     */
    @ApiOperation("查询列表")
    @HasPermission({"sms-batch:query"})
    @GetMapping("/query")
    public InvokeResult<PageResult<QuerySmsBatchBo>> query(@Valid QuerySmsBatchVo vo) {

        Wrapper<SmsBatchDetail> queryWrapper = Wrappers.lambdaQuery(SmsBatchDetail.class)
            .eq(StringUtil.isNotBlank(vo.getBatchNo()), SmsBatchDetail::getBatchNo, vo.getBatchNo())
            .eq(vo.getMchId() != null, SmsBatchDetail::getMchId, vo.getMchId())
            .eq(StringUtil.isNotBlank(vo.getPhoneNumber()), SmsBatchDetail::getPhoneNumber,
                vo.getPhoneNumber())
            .ge(vo.getStartTime() != null, SmsBatchDetail::getCreateTime, vo.getStartTime())
            .le(vo.getEndTime() != null, SmsBatchDetail::getCreateTime, vo.getEndTime())
            .eq(vo.getStatus() != null, SmsBatchDetail::getStatus, vo.getStatus())
            .orderByDesc(SmsBatchDetail::getCreateTime);

        Page<SmsBatchDetail> page = smsBatchDetailService.page(
            new Page<>(vo.getPageIndex(), vo.getPageSize()), queryWrapper);
        PageResult<SmsBatchDetail> pageResult = PageResultUtil.convert(page);

        List<SmsBatchDetail> datas = pageResult.getDatas();
        List<QuerySmsBatchBo> results = null;

        if (!CollectionUtil.isEmpty(datas)) {
            results = datas.stream().map(QuerySmsBatchBo::new).collect(Collectors.toList());
        }

        return InvokeResultBuilder.success(PageResultUtil.rebuild(pageResult, results));
    }

    /**
     * 查询详情
     */
    @ApiOperation("查询详情")
    @ApiImplicitParam(value = "ID", name = "id", paramType = "query", required = true)
    @HasPermission({"sms-batch:query"})
    @GetMapping
    public InvokeResult<GetSmsBatchBo> get(@NotBlank(message = "ID不能为空！") String id) {

        SmsBatchDetail smsBatchDetail = smsBatchDetailService.getById(id);
        if (smsBatchDetail == null) {
            throw new DefaultClientException("短信批次详情不存在！");
        }

        return InvokeResultBuilder.success(new GetSmsBatchBo(smsBatchDetail));
    }
}
