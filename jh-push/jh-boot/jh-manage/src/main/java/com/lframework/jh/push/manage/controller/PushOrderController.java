package com.lframework.jh.push.manage.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lframework.jh.push.core.annotations.security.HasPermission;
import com.lframework.jh.push.core.components.resp.InvokeResult;
import com.lframework.jh.push.core.components.resp.InvokeResultBuilder;
import com.lframework.jh.push.core.components.resp.PageResult;
import com.lframework.jh.push.core.exceptions.impl.DefaultClientException;
import com.lframework.jh.push.core.utils.CollectionUtil;
import com.lframework.jh.push.core.utils.PageResultUtil;
import com.lframework.jh.push.core.utils.StringUtil;
import com.lframework.jh.push.manage.bo.order.GetPushOrderBo;
import com.lframework.jh.push.manage.bo.order.QueryPushOrderBo;
import com.lframework.jh.push.service.entity.PushOrder;
import com.lframework.jh.push.service.service.PushOrderService;
import com.lframework.jh.push.service.vo.order.QueryPushOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 推送单查询
 *
 * @author zmj
 */
@Api(tags = "推送单查询")
@Validated
@RestController
@RequestMapping("/order")
public class PushOrderController {

    @Autowired
    private PushOrderService pushOrderService;

    /**
     * 查询列表
     */
    @ApiOperation("查询列表")
    @HasPermission({"push-order:query"})
    @GetMapping("/query")
    public InvokeResult<PageResult<QueryPushOrderBo>> query(@Valid QueryPushOrderVo vo) {

        Wrapper<PushOrder> queryWrapper = Wrappers.lambdaQuery(PushOrder.class)
            .eq(StringUtil.isNotBlank(vo.getId()), PushOrder::getId, vo.getId())
            .eq(vo.getMchId() != null, PushOrder::getMchId, vo.getMchId())
            .eq(StringUtil.isNotBlank(vo.getAppId()), PushOrder::getAppId, vo.getAppId())
            .eq(StringUtil.isNotBlank(vo.getChannel()), PushOrder::getChannel, vo.getChannel())
            .eq(StringUtil.isNotBlank(vo.getOuterNo()), PushOrder::getOuterNo, vo.getOuterNo())
            .eq(StringUtil.isNotBlank(vo.getBatchNo()), PushOrder::getBatchNo, vo.getBatchNo())
            .ge(vo.getStartTime() != null, PushOrder::getCreateTime, vo.getStartTime())
            .le(vo.getEndTime() != null, PushOrder::getCreateTime, vo.getEndTime())
            .eq(vo.getOrderType() != null, PushOrder::getOrderType, vo.getOrderType())
            .eq(vo.getStatus() != null, PushOrder::getStatus, vo.getStatus())
            .orderByDesc(PushOrder::getCreateTime);

        Page<PushOrder> page = pushOrderService.page(
            new Page<>(vo.getPageIndex(), vo.getPageSize()), queryWrapper);
        PageResult<PushOrder> pageResult = PageResultUtil.convert(page);

        List<PushOrder> datas = pageResult.getDatas();
        List<QueryPushOrderBo> results = null;

        if (!CollectionUtil.isEmpty(datas)) {
            results = datas.stream().map(QueryPushOrderBo::new).collect(Collectors.toList());
        }

        return InvokeResultBuilder.success(PageResultUtil.rebuild(pageResult, results));
    }

    /**
     * 查询详情
     */
    @ApiOperation("查询详情")
    @ApiImplicitParam(value = "ID", name = "id", paramType = "query", required = true)
    @HasPermission({"push-order:query"})
    @GetMapping
    public InvokeResult<GetPushOrderBo> get(@NotBlank(message = "ID不能为空！") String id) {

        PushOrder pushOrder = pushOrderService.getById(id);
        if (pushOrder == null) {
            throw new DefaultClientException("推送单不存在！");
        }

        return InvokeResultBuilder.success(new GetPushOrderBo(pushOrder));
    }
}
