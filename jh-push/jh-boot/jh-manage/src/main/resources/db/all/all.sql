SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for op_logs
-- ----------------------------
DROP TABLE IF EXISTS `op_logs`;
CREATE TABLE `op_logs`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `name`         varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日志名称',
    `log_type`     tinyint(3) NOT NULL COMMENT '类别',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `extra`        longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '补充信息',
    `ip`           varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'IP地址',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX          `create_by`(`create_by`) USING BTREE,
    INDEX          `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_logs
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `id`             varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `code`           varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '编号',
    `name`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称（前端使用）',
    `title`          varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '标题',
    `icon`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
    `component_type` tinyint(3) NULL DEFAULT NULL COMMENT '组件类型（前端使用）',
    `component`      varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件（前端使用）',
    `request_param`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '自定义请求参数',
    `parent_id`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级ID',
    `path`           varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由路径（前端使用）',
    `no_cache`       tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否缓存（前端使用）',
    `display`        tinyint(3) NOT NULL COMMENT '类型 0-目录 1-菜单 2-功能',
    `hidden`         tinyint(1) NULL DEFAULT 0 COMMENT '是否隐藏（前端使用）',
    `permission`     varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限',
    `is_special`     tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否特殊菜单',
    `available`      tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
    `description`    varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `create_by`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_by_id`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`    datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_by_id`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`    datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX            `code`(`code`, `name`, `title`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu`
VALUES ('0001', '0001', 'JhPush', '工作台', 'a-menu', NULL, '', NULL, NULL, '/jh', 0, 0, 0, '', 1,
        1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000', '1000', 'System', '系统管理', 'a-menu', NULL, '', NULL, '0001', '/system', 0, 0, 0,
        '', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000001', '1000001', 'Menu', '菜单管理', 'a-menu', 0, '/system/menu/index', NULL, '1000',
        '/menu', 0, 1, 0, 'system:menu:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00',
        '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000001001', '1000001001', '', '新增菜单', NULL, 0, '', NULL, '1000001', '', 0, 2, 0,
        'system:menu:add', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000001002', '1000001002', '', '修改菜单', NULL, 0, '', NULL, '1000001', '', 0, 2, 0,
        'system:menu:modify', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000001003', '1000001003', '', '删除菜单', NULL, 0, '', NULL, '1000001', '', 0, 2, 0,
        'system:menu:delete', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000004', '1000004', 'Role', '角色管理', 'a-menu', 0, '/system/role/index', NULL, '1000',
        '/role', 0, 1, 0, 'system:role:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00',
        '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000004001', '1000004001', '', '新增角色', NULL, 0, '', NULL, '1000004', '', 0, 2, 0,
        'system:role:add', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000004002', '1000004002', '', '修改角色', NULL, 0, '', NULL, '1000004', '', 0, 2, 0,
        'system:role:modify', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000004003', '1000004003', '', '角色授权', NULL, 0, '', NULL, '1000004', '', 0, 2, 0,
        'system:role:permission', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员',
        '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000005', '1000005', 'User', '用户管理', 'a-menu', 0, '/system/user/index', NULL, '1000',
        '/user', 0, 1, 0, 'system:user:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00',
        '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000005001', '1000005001', '', '新增用户', NULL, 0, '', NULL, '1000005', '', 0, 2, 0,
        'system:user:add', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000005002', '1000005002', '', '修改用户', NULL, 0, '', NULL, '1000005', '', 0, 2, 0,
        'system:user:modify', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000005003', '1000005003', '', '用户授权', NULL, 0, '', NULL, '1000005', '', 0, 2, 0,
        'system:user:permission', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员',
        '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1000006', '1000006', 'Oplog', '操作日志', 'a-menu', 0, '/system/oplog/index', NULL, '1000',
        '/oplog', 0, 1, 0, 'system:oplog:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00',
        '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1005', '1005', 'Mch', '商户管理', 'a-menu', 0, '/mch/index', NULL, '0001', '/mch', 0, 1, 0,
        'mch:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1005001', '1010001', '', '新增商户', NULL, 0, '', NULL, '1010', '', 0, 2, 0, 'mch:add', 1,
        1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1005002', '1010002', '', '修改商户', NULL, 0, '', NULL, '1010', '', 0, 2, 0, 'mch:modify',
        1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1010', '1010', 'App', '应用管理', 'a-menu', 0, '/app/index', NULL, '0001', '/app', 0, 1, 0,
        'app:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1010001', '1010001', '', '新增应用', NULL, 0, '', NULL, '1010', '', 0, 2, 0, 'app:add', 1,
        1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1010002', '1010002', '', '修改应用', NULL, 0, '', NULL, '1010', '', 0, 2, 0, 'app:modify',
        1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1010003', '1010003', '', '配置渠道', NULL, 0, '', NULL, '1010', '', 0, 2, 0,
        'app:set-channel', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1020', '1020', 'Channel', '渠道管理', 'a-menu', 0, '/channel/index', NULL, '0001',
        '/channel', 0, 1, 0, 'channel:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00',
        '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1020001', '1020001', '', '新增渠道', NULL, 0, '', NULL, '1020', '', 0, 2, 0, 'channel:add',
        1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1020002', '1020002', '', '修改渠道', NULL, 0, '', NULL, '1020', '', 0, 2, 0,
        'channel:modify', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1030', '1030', 'PushOrder', '推送单查询', 'a-menu', 0, '/push-order/index', NULL, '0001',
        '/push-order', 0, 1, 0, 'push-order:query', 1, 1, '', '系统管理员', '1',
        '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1040', '1040', 'Batch', '批次查询', 'a-menu', NULL, '', NULL, '0001', '/batch', 0, 0, 0,
        'sms-batch:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1040001', '1040001', 'SmsBatch', '短信批次查询', 'a-menu', 0, '/sms-batch/index', NULL,
        '1040', '/sms', 0, 1, 0, 'sms-batch:query', 1, 1, '', '系统管理员', '1',
        '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1050', '1050', 'NotifyData', '回调通知', 'a-menu', 0, '/notify-data/index', NULL, '0001',
        '/notify-data', 0, 1, 0, 'notify-data:query', 1, 1, '', '系统管理员', '1',
        '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu`
VALUES ('1050001', '1050001', '', '重试通知', NULL, 0, '', NULL, '1050', '', 0, 2, 0,
        'notify-data:retry', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');

-- ----------------------------
-- Table structure for sys_menu_collect
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_collect`;
CREATE TABLE `sys_menu_collect`
(
    `id`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
    `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
    `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `user_id, menu_id`(`user_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单收藏' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu_collect
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `code`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '编号',
    `name`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '名称',
    `permission`   varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限',
    `available`    tinyint(1) NOT NULL COMMENT '状态',
    `description`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人',
    `update_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `code`(`code`) USING BTREE,
    UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role`
VALUES ('1', '001', '系统管理员', 'admin', 1, '系统管理员', '系统管理员', '1',
        '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `id`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
    `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
    `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `role_id, menu_id`(`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `code`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '编号',
    `name`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '姓名',
    `username`     varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '用户名',
    `password`     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
    `email`        varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
    `telephone`    varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
    `gender`       tinyint(3) NOT NULL DEFAULT 0 COMMENT '性别 0-未知 1-男 2-女',
    `available`    tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1-在用 0停用',
    `lock_status`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '锁定状态',
    `description`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人',
    `update_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `code`(`code`) USING BTREE,
    UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user`
VALUES ('1', '001', '系统管理员', 'admin',
        '$2a$10$IJtHluhnhAYkgvM4PdKuZek5PWbtuxtjB9pB.twZdxg/qrlR4s4q6', 'jhpush@lframework.com',
        '17600000001', 0, 1, 0, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `id`      varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
    `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
    `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `user_id, role_id`(`user_id`, `role_id`) USING BTREE,
    INDEX     `role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role`
VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for tbl_app
-- ----------------------------
DROP TABLE IF EXISTS `tbl_app`;
CREATE TABLE `tbl_app`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `name`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '名称',
    `available`    tinyint(1) NOT NULL COMMENT '状态',
    `description`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `mch_id`       int(11) NOT NULL COMMENT '商户ID',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人',
    `update_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `name`(`name`) USING BTREE,
    INDEX          `mch_id`(`mch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '应用' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_app
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_app_channel
-- ----------------------------
DROP TABLE IF EXISTS `tbl_app_channel`;
CREATE TABLE `tbl_app_channel`
(
    `id`         varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
    `app_id`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用ID',
    `channel_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '渠道ID',
    `params`     longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数',
    `available`  tinyint(1) NOT NULL COMMENT '状态',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `app_id`(`app_id`, `channel_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '应用和渠道关系表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_app_channel
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_channel
-- ----------------------------
DROP TABLE IF EXISTS `tbl_channel`;
CREATE TABLE `tbl_channel`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `code`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '编号',
    `name`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '名称',
    `params`       longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置参数',
    `available`    tinyint(1) NOT NULL COMMENT '状态',
    `description`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人',
    `update_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `code`(`code`) USING BTREE,
    UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '渠道' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_channel
-- ----------------------------
INSERT INTO `tbl_channel`
VALUES ('1', 'ALI_SMS', '阿里云短信',
        '[{\"paramName\":\"accessKey\",\"paramKey\":\"accessKeyId\",\"isRequired\":true},{\"paramName\":\"secret\",\"paramKey\":\"accessKeySecret\",\"isRequired\":true}]',
        1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `tbl_channel`
VALUES ('2', 'TX_SMS', '腾讯云短信',
        '[{\"paramName\":\"secretId\",\"paramKey\":\"secretId\",\"isRequired\":true},{\"paramName\":\"secretKey\",\"paramKey\":\"secretKey\",\"isRequired\":true},{\"paramName\":\"地域\",\"paramKey\":\"region\",\"isRequired\":false},{\"paramName\":\"应用ID\",\"paramKey\":\"sdkAppId\",\"isRequired\":true}]',
        1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 21:35:58');

-- ----------------------------
-- Table structure for tbl_mch
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mch`;
CREATE TABLE `tbl_mch`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '名称',
    `available`    tinyint(1) NOT NULL COMMENT '状态',
    `description`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `api_secret`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通信秘钥',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人',
    `update_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10000 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商户' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_mch
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_notify_data
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notify_data`;
CREATE TABLE `tbl_notify_data`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `mch_id`       int(11) NOT NULL COMMENT '商户ID',
    `notify_url`   varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '通知URL',
    `batch_no`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '批次号',
    `content`      longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '通知内容',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `notify_count` int(11) NOT NULL DEFAULT 0 COMMENT '推送次数',
    `resp`         longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '第三方响应值',
    PRIMARY KEY (`id`) USING BTREE,
    INDEX          `batch_no`(`batch_no`) USING BTREE,
    INDEX          `mch_id`(`mch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_notify_data
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_push_order
-- ----------------------------
DROP TABLE IF EXISTS `tbl_push_order`;
CREATE TABLE `tbl_push_order`
(
    `id`             varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `mch_id`         int(11) NOT NULL COMMENT '商户ID',
    `app_id`         varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '应用ID',
    `channel_id`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '渠道ID',
    `channel`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '渠道',
    `phone_numbers`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '接收手机号',
    `outer_no`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '外部单号',
    `channel_params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '渠道参数',
    `notify_url`     varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通知Url',
    `biz_id`         varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '业务ID',
    `param1`         varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数1',
    `param2`         varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数2',
    `extra`          longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展参数',
    `order_type`     tinyint(3) NOT NULL COMMENT '单据类型',
    `status`         tinyint(3) NOT NULL COMMENT '状态',
    `create_time`    datetime                                                      NOT NULL COMMENT '创建时间',
    `update_time`    datetime                                                      NOT NULL COMMENT '修改时间',
    `channel_resp`   longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '渠道响应数据',
    `success_time`   datetime NULL DEFAULT NULL COMMENT '成功时间',
    `failure_time`   datetime NULL DEFAULT NULL COMMENT '失败时间',
    `error_msg`      longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '失败原因',
    `batch_no`       varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '批次号',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `app_id`(`app_id`, `outer_no`) USING BTREE,
    INDEX            `channel_id`(`channel_id`) USING BTREE,
    INDEX            `outer_no`(`outer_no`) USING BTREE,
    INDEX            `mch_id`(`mch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '推送单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_push_order
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_sms_batch_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sms_batch_detail`;
CREATE TABLE `tbl_sms_batch_detail`
(
    `id`           varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT 'ID',
    `mch_id`       int(11) NOT NULL COMMENT '商户ID',
    `batch_no`     varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '批次号',
    `phone_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
    `status`       tinyint(3) NOT NULL COMMENT '状态',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    `success_time` datetime NULL DEFAULT NULL COMMENT '成功时间',
    `sms_content`  longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '短信内容',
    `error_msg`    longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '错误原因',
    `biz_id`       varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '业务ID',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `batch_no`(`batch_no`, `phone_number`) USING BTREE,
    INDEX          `mch_id`(`mch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信批次明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_sms_batch_detail
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;