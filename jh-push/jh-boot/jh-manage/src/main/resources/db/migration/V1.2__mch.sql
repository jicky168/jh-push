SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_mch
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mch`;
CREATE TABLE `tbl_mch`
(
    `id`           int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`         varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '名称',
    `available`    tinyint(1) NOT NULL COMMENT '状态',
    `description`  varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
    `api_secret`   varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通信秘钥',
    `create_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人',
    `create_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '创建人ID',
    `create_time`  datetime                                                      NOT NULL COMMENT '创建时间',
    `update_by`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人',
    `update_by_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NOT NULL COMMENT '修改人ID',
    `update_time`  datetime                                                      NOT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10000 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商户' ROW_FORMAT = Dynamic;

insert into tbl_mch (name, available, api_secret, create_by, create_by_id, create_time, update_by,
                     update_by_id, update_time)
select name,
       available,
       api_secret,
       create_by,
       create_by_id,
       create_time,
       update_by,
       update_by_id,
       update_time
from tbl_app;

ALTER TABLE `tbl_app`
    ADD COLUMN `mch_id` int(11) NOT NULL COMMENT '商户ID' AFTER `description`;

update tbl_app as a join tbl_mch as m
on m.api_secret = a.api_secret
    set a.mch_id = m.id;

ALTER TABLE `tbl_app`
DROP
COLUMN `api_secret`;

ALTER TABLE `tbl_app`
    ADD INDEX `mch_id`(`mch_id`) USING BTREE;

INSERT INTO `sys_menu` (`id`, `code`, `name`, `title`, `icon`, `component_type`, `component`,
                        `request_param`, `parent_id`, `path`, `no_cache`, `display`, `hidden`,
                        `permission`, `is_special`, `available`, `description`, `create_by`,
                        `create_by_id`, `create_time`, `update_by`, `update_by_id`, `update_time`)
VALUES ('1005', '1005', 'Mch', '商户管理', 'a-menu', 0, '/mch/index', NULL, '0001', '/mch', 0, 1, 0,
        'mch:query', 1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');
INSERT INTO `sys_menu` (`id`, `code`, `name`, `title`, `icon`, `component_type`, `component`,
                        `request_param`, `parent_id`, `path`, `no_cache`, `display`, `hidden`,
                        `permission`, `is_special`, `available`, `description`, `create_by`,
                        `create_by_id`, `create_time`, `update_by`, `update_by_id`, `update_time`)
VALUES ('1005001', '1010001', '', '新增商户', NULL, 0, '', NULL, '1010', '', 0, 2, 0, 'mch:add', 1,
        1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1', '2023-08-20 00:00:00');
INSERT INTO `sys_menu` (`id`, `code`, `name`, `title`, `icon`, `component_type`, `component`,
                        `request_param`, `parent_id`, `path`, `no_cache`, `display`, `hidden`,
                        `permission`, `is_special`, `available`, `description`, `create_by`,
                        `create_by_id`, `create_time`, `update_by`, `update_by_id`, `update_time`)
VALUES ('1005002', '1010002', '', '修改商户', NULL, 0, '', NULL, '1010', '', 0, 2, 0, 'mch:modify',
        1, 1, '', '系统管理员', '1', '2023-08-20 00:00:00', '系统管理员', '1',
        '2023-08-20 00:00:00');

ALTER TABLE `tbl_push_order`
    ADD COLUMN `mch_id` int(11) NOT NULL COMMENT '商户ID' AFTER `id`,
ADD INDEX `mch_id`(`mch_id`) USING BTREE;



ALTER TABLE `tbl_sms_batch_detail`
    ADD COLUMN `mch_id` int(11) NOT NULL COMMENT '商户ID' AFTER `id`,
ADD INDEX `mch_id`(`mch_id`) USING BTREE;

ALTER TABLE `tbl_notify_data`
    ADD COLUMN `mch_id` int(11) NOT NULL COMMENT '商户ID' AFTER `id`,
ADD INDEX `mch_id`(`mch_id`) USING BTREE;

update tbl_push_order as o join tbl_app as a on a.id = o.app_id
    set o.mch_id = a.mch_id;
update tbl_sms_batch_detail as d join tbl_push_order as o on d.batch_no = o.batch_no
    set d.mch_id = o.mch_id;
update tbl_notify_data as d join tbl_push_order as o on d.batch_no = o.batch_no
    set d.mch_id = o.mch_id;
SET
FOREIGN_KEY_CHECKS = 1;
