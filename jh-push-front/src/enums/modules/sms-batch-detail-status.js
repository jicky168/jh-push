const SMS_BATCH_DETAIL_STATUS = {
  CREATED: {
    code: 0,
    desc: '待推送'
  },
  SUCCESS: {
    code: 6,
    desc: '推送成功'
  },
  FAIL: {
    code: 9,
    desc: '推送失败'
  }
}

export default SMS_BATCH_DETAIL_STATUS
