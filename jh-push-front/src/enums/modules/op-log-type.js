/**
 * 操作日志类型
 * @type {{AUTH: {code: number, desc: string}, OTHER: {code: number, desc: string}}}
 */
const OP_LOG_TYPE = {
  AUTH: {
    code: 1,
    desc: '用户认证'
  },
  APP: {
    code: 2,
    desc: '应用'
  },
  CHANNEL: {
    code: 3,
    desc: '渠道'
  },
  MCH: {
    code: 4,
    desc: '商户'
  },
  SYS: {
    code: 99,
    desc: '系统管理'
  }
}

export default OP_LOG_TYPE
