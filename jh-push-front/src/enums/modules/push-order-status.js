const PUSH_ORDER_STATUS = {
  CREATED: {
    code: 0,
    desc: '待推送'
  },
  PUSHING: {
    code: 3,
    desc: '推送中'
  },
  SUCCESS: {
    code: 6,
    desc: '推送完成'
  },
  FAIL: {
    code: 9,
    desc: '推送失败'
  }
}

export default PUSH_ORDER_STATUS
