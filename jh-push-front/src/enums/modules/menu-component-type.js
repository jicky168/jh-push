/**
 * 菜单组件类型
 * @type {{CUSTOM_LIST: {code: number, desc: string}, NORMAL: {code: number, desc: string}}}
 */
const MENU_COMPONENT_TYPE = {
  NORMAL: {
    code: 0,
    desc: '普通'
  }
}

export default MENU_COMPONENT_TYPE
