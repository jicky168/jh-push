import JForm from '@/components/JForm'
import JFormItem from '@/components/JFormItem'
import JBorder from '@/components/JBorder'
import SvgIcon from '@/components/SvgIcon'
import IconPicker from '@/components/IconPicker'
import CronPicker from '@/components/CronPicker'

const instance = {}
instance.install = function(Vue) {
  Vue.component('JForm', JForm)
  Vue.component('JFormItem', JFormItem)
  Vue.component('JBorder', JBorder)
  Vue.component('SvgIcon', SvgIcon)
  Vue.component('IconPicker', IconPicker)
  Vue.component('CronPicker', CronPicker)

  // selector
  const selectors = require.context('./Selector', true, /\.vue$/i)
  selectors.keys().forEach(modulePath => {
    const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
    const value = selectors(modulePath)
    Vue.component(moduleName, value.default)
  })

  // tag
  const tags = require.context('./Tag', true, /\.vue$/i)
  tags.keys().forEach(modulePath => {
    const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
    const value = tags(modulePath)
    Vue.component(moduleName, value.default)
  })
}

export default instance
