import { request } from '@/utils/request'

export default {
  /**
   * 查询列表
   * @param params
   * @returns {AxiosPromise}
   */
  query: (params) => {
    return request({
      url: '/notify/data/query',
      region: 'cloud-api',
      method: 'get',
      params: params
    })
  },
  /**
   * 根据ID查询
   * @param id
   * @returns {AxiosPromise}
   */
  get: (id) => {
    return request({
      url: '/notify/data',
      region: 'cloud-api',
      method: 'get',
      params: {
        id: id
      }
    })
  },
  /**
   * 重新通知
   * @param id
   * @returns {*}
   */
  retryNotify: (id) => {
    return request({
      url: '/notify/data/retry',
      region: 'cloud-api',
      method: 'post',
      params: {
        id: id
      }
    })
  }
}
