import { request } from '@/utils/request'

export default {
  /**
   * 查询列表
   * @param params
   * @returns {AxiosPromise}
   */
  query: (params) => {
    return request({
      url: '/app/channel/query',
      region: 'cloud-api',
      method: 'get',
      params: params
    })
  },
  /**
   * 查询参数
   * @param params
   * @returns {AxiosPromise}
   */
  getParams: (params) => {
    return request({
      url: '/app/channel/params',
      region: 'cloud-api',
      method: 'get',
      params: params
    })
  },
  /**
   * 配置参数
   * @param params
   * @returns {AxiosPromise}
   */
  setParams: (data) => {
    return request({
      url: '/app/channel/params',
      region: 'cloud-api',
      method: 'post',
      dataType: 'json',
      data: data
    })
  }
}
