import { request } from '@/utils/request'

export default {
  /**
   * 今日推送单
   * @param params
   * @returns {AxiosPromise}
   */
  todayOrder: () => {
    return request({
      url: '/chart/order/today',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 今日推送单折线图
   * @param params
   * @returns {AxiosPromise}
   */
  todayOrderLine: () => {
    return request({
      url: '/chart/order/today/line',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 本月推送单
   * @param params
   * @returns {AxiosPromise}
   */
  monthOrder: () => {
    return request({
      url: '/chart/order/month',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 本月推送单折线图
   * @param params
   * @returns {AxiosPromise}
   */
  monthOrderLine: () => {
    return request({
      url: '/chart/order/month/line',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 今日短信数量
   * @param params
   * @returns {AxiosPromise}
   */
  todaySms: () => {
    return request({
      url: '/chart/sms/today',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 今日短信数量折线图
   * @param params
   * @returns {AxiosPromise}
   */
  todaySmsLine: () => {
    return request({
      url: '/chart/sms/today/line',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 本月短信数量
   * @param params
   * @returns {AxiosPromise}
   */
  monthSms: () => {
    return request({
      url: '/chart/sms/month',
      region: 'cloud-api',
      method: 'get'
    })
  },
  /**
   * 本月短信数量折线图
   * @param params
   * @returns {AxiosPromise}
   */
  monthSmsLine: () => {
    return request({
      url: '/chart/sms/month/line',
      region: 'cloud-api',
      method: 'get'
    })
  }
}
